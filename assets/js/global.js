var base_url = window.location.origin+'/scholarship_information/';
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

$(function($) {
      // this bit needs to be loaded on every page where an ajax POST may happen
      $.ajaxSetup({
          data: {
              token: getCookie('csrf_cookie')
          }
      });
      // now you can use plain old POST requests like always
  });
function set_csrf(name)
{
  $(function($) {
      // this bit needs to be loaded on every page where an ajax POST may happen
      $.ajaxSetup({
          data: {
              token: getCookie('csrf_cookie')
          }
      });
      // now you can use plain old POST requests like always
  });

  $(document).on("click",".signout",function(e)
  {
    e.preventDefault();
    swal(
      "wew",
      "weq",
      "info"
    )
  })
}

function notify2(header,mes,mes_type)
{
    swal(
      header,
      mes,
      mes_type
    )
}

function loading()
{
  swal({
     title: 'Please wait...',
     allowOutsideClick: false,
     allowEscapeKey: false

  });
  swal.showLoading();
}

function close_loading()
{
  swal.close();
}

$(function($) {
    $(document).on("click",".signout",function(e)
    {
      e.preventDefault();
      $.ajaxSetup({
          data: {
              token: getCookie('csrf_cookie')
          }
      });
      var post_url = base_url+'Logout/index';
      swal({
      title: 'Are you sure to logout?',
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
      }).then((result) => {
          $.ajax({
                type : 'POST',
                url : post_url,
                data:{},
                dataType:"json",
                beforeSend:function(){
                  loading();
                },
                success : function(res){
                  close_loading();
                  window.location.href= res.url; // the redirect goes here
                },
                error : function(res) {
                    console.log(res);
                    notify2("Failed","error found","error");
                }
            });
      });
    })
});

  