<?php
class Crud_model extends CI_Model{

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}


	public function fetch($table,$where="",$limit="",$offset="",$order=""){
			if (!empty($where)) {
				$this->db->where($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}

		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return $query->result();
		}else{
			return FALSE;
		}
	}

	public function fetch_data($table,$where="",$limit="",$offset="",$order="",$group=""){
			if (!empty($where)) {
				$this->db->where($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}
			if (!empty($group)) {
				$this->db->group_by($group); 
			}

		$query = $this->db->get($table);
		return $query->result();

	}


	public function fetch_tag($tag,$table,$where="",$limit="",$offset="",$order=""){
			if (!empty($where)) {
				$this->db->where($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}
			$this->db->select($tag);
			$this->db->from($table);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->result();
			}else{
				return FALSE;
			}
	}

	public function fetch_tag_array($tag,$table,$where="",$limit="",$offset="",$order=""){
			if (!empty($where)) {
				$this->db->where($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}
			$this->db->select($tag);
			$this->db->from($table);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->result_array();
			}else{
				return FALSE;
			}
	}


	public function fetch_tag_row($tag,$table,$where="",$limit="",$offset="",$order=""){
			if (!empty($where)) {
				$this->db->where($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}
			$this->db->select($tag);
			$this->db->from($table);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->row();
			}else{
				return FALSE;
			}
	}

	public function fetch_tag_row_like($tag,$table,$where="",$limit="",$offset="",$order=""){
			if (!empty($where)) {
				$this->db->like($where);	
			}
			if (!empty($limit)) {
				if (!empty($offset)) {
					$this->db->limit($limit, $offset);
				}else{
					$this->db->limit($limit);	
				}
			}
			if (!empty($order)) {
				$this->db->order_by($order); 
			}
			$this->db->select($tag);
			$this->db->from($table);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->row();
			}else{
				return FALSE;
			}
	}
	public function fetch_tag2($tag,$table,$where=""){
			$query = $this->db->select($tag)->from($table)->where($where)->get()->row();
			if($query)
				return $query->$tag;
			else
				return false;

	}

	public function insert($table,$data){
		$result = $this->db->insert($table,$data);
		if ($result) {
				return TRUE;
			}else{
				return FALSE;
			}
	}


	public function insert_id($table,$data){
		$result = $this->db->insert($table,$data);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function update($table,$data,$where=""){
		if($where!=""){
				$this->db->where($where);
			}
		$result = $this->db->update($table,$data);
		if ($this->db->affected_rows()) {
				return TRUE;
			}else{
				return FALSE;
			}
	}

	public function delete($table,$where=""){
		if($where!=""){
				$this->db->where($where);
			}
	 	$result = $this->db->delete($table); 
	 		if ($result) {
				return TRUE;
			}else{
				return FALSE;
			}
	}

	function check_exist($table,$filter)
	{
		return $this->db->select('id')->from($table)->where($filter)->get()->num_rows();

	}

	public function count_records($table,$where="")
	{
		if($where!=""){
			$this->db->where($where);
		}
	 	return $this->db->count_all_results($table);
	}

	public function getScholars()
	{
		$query = $this->db->select("a.status as scholar_status,a.created_at as scholar_created,a.id as scholar_id,a.student_number as student_number,a.curriculum_id as curriculum_id,
			,a.section as section,a.academic_year_from as scholar_academic_year_from, a.academic_year_to as scholar_academic_year_to,
			,b.*,c.scholar_type as scholar_type,d.curriculum as curriculum")
		->from("scholars a")
		->join("examinations b", "b.id = a.examinations_id", "left")
		->join("scholarship c", "c.id = a.scholarship_type", "left")
		->join("curriculum d", "d.id = a.curriculum_id", "left")
		->get();

		return $query->result();
	}

	public function getScholars_sort($filter)
	{
		$query = $this->db->select("a.status as scholar_status,a.created_at as scholar_created,a.id as scholar_id,a.student_number as student_number,a.curriculum_id as curriculum_id,
			,a.section as section,a.academic_year_from as scholar_academic_year_from, a.academic_year_to as scholar_academic_year_to,
			,b.*,c.scholar_type as scholar_type,d.curriculum as curriculum")
		->from("scholars a")
		->join("examinations b", "b.id = a.examinations_id", "left")
		->join("scholarship c", "c.id = a.scholarship_type", "left")
		->join("curriculum d", "d.id = a.curriculum_id", "left")
		->where($filter)
		->get();

		return $query->result();
	}

	public function getScholar($filter)
	{
		$query = $this->db->select("a.status as scholar_status,a.created_at as scholar_created,a.id as scholar_id,a.student_number as student_number,
			,a.section as section,a.academic_year_from as scholar_academic_year_from, a.academic_year_to as scholar_academic_year_to,a.contact_number as scholar_contact_number,
			a.curriculum_id as curriculum_id,a.scholarship_type as scholarship_id,b.*,c.scholar_type as scholar_type,d.curriculum as curriculum,d.subjects as subjects")
		->from("scholars a")
		->join("examinations b", "b.id = a.examinations_id", "left")
		->join("scholarship c", "c.id = a.scholarship_type", "left")
		->join("curriculum d", "d.id = a.curriculum_id", "left")
		->where($filter)
		->get();

		return $query->row();
	}

	function get_scholar_chart($filter)
	{
		$query = $this->db->select("count(a.id) as count_scholar,b.scholar_type as scholar_type")
		->from("scholars a")
		->where($filter)
		->group_by("a.scholarship_type", "asc")
		->join("scholarship b","b.id=a.scholarship_type","left")		
		->get();
		return $query->result();
	}

	function get_audit_trails()
	{
		$query = $this->db->select("a.*,b.email_address as email")
		->from("audit_trails a")
		->join("users b","b.id=a.user_id","left")		
		->order_by("a.created","desc")
		->get();
		return $query->result();
	}

	function get_top10_scholars()
	{
		$academic_year_to = date("Y");
		$academic_year_from = $academic_year_to-1;
		$academic_year = $academic_year_from."-".$academic_year_to;
		$query = $this->db->select("a.latest_avg as latest_avg,c.first_name as first_name, c.last_name as last_name, c.middle_name as middle_name
			,c.suffix as suffix")
		->from("scholar_grades a")
		->where("a.status",0)
		->where("a.academic_year",$academic_year)
		->join("scholars b","b.id = a.scholar_id","left")
		->join("examinations c","c.id = b.examinations_id","left")
		->order_by("a.latest_avg","desc")
		->limit(10)
		->get();

		return $query->result();
	}

	function print_scholars($filter)
	{
		$query = $this->db->select("a.student_number as student_number,c.first_name as first_name, c.last_name as last_name, c.middle_name as middle_name
			,c.suffix as suffix,b.scholar_type")
		->from("scholars a")
		->where("a.status",1)
		->where($filter)
		->join("scholarship b","b.id = a.scholarship_type","left")
		->join("examinations c","c.id =a.examinations_id","left")
		->get();

		return $query->result();

	}

	function get_exam_schools()
	{
		$query = $this->db->select("Distinct(LOWER(school)) as school")
		->from("examinations")
		->order_by("school","asc")
		->get();

		return $query->result();
	}

	function get_examination_results($school,$filter)
	{
		$query = $this->db->select("*")
		->from("examinations")
		->where($filter)
		->where("school",$school)
		->order_by("last_name","asc")
		->get();

		return $query->result();
	}
}