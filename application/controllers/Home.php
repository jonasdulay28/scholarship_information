<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Crud_model");
		if(logged_in($this))
		{
			redirect('Dashboard');
		}
	}

	public function index()
	{
		if($_POST)
		{
			$email = strtolower(clean_data($this->input->post('email')));
			$password=  clean_data($this->input->post('password'));
			$filter = array("email_address"=>$email,"status"=>1);
			$user_details = $this->Crud_model->fetch_tag_row("first_name,last_name,middle_name,suffix,id,password,role","users",$filter);
			if(empty($user_details))
			{
				$res["message"] = "Invalid email or password.";
			}
			else
			{
				$user_password = $user_details->password;
				if(password_verify($password,$user_password))
				{
					//success login
					$res["message"] = 'success';
					$res["url"] = base_url('Dashboard');
					//get account info
					$middle_initial = substr($user_details->middle_name,0,1).'.';
					$full_name = $user_details->last_name.', '.$user_details->first_name.' '.$middle_initial.' '.$user_details->suffix;
					$sess_id = encrypt($user_details->id);
					$sess_name = encrypt($full_name);
					$sess_role = encrypt($user_details->role);
					$sessdata = array(
						        'id'  => $sess_id,
						        'name'     => $sess_name,
						        'role' => $sess_role
							);

					$this->session->set_userdata($sessdata);

					//insert audit trails
					//Audit trails 
					$audit = array("action" => "Logged in", "description"=>"Logged in", "user_id"=>$user_details->id,
						"ip_address"=>$this->input->ip_address());
					$this->Crud_model->insert("audit_trails",$audit);
				}
				else
				{
					$res["message"] = "Invalid email or password";
				}
			}
			echo json_encode($res);
		}
		else
		{
		   $this->load->view('pages/homepage');
		}
		
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}
	
}