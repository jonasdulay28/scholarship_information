<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$filter = array("status" => 1);
		$data["data"]['curriculum'] = $this->Crud_model->fetch_tag("id,curriculum","curriculum",$filter);
		$data["data"]['scholarship'] = $this->Crud_model->fetch_tag("id,scholar_type","scholarship",$filter);
		$this->load->view('templates/monitoring_template',$data);
	}

	
	public function records()
	{
		$filter = array("status" => 1);
		$data["curriculum"] = $this->Crud_model->fetch_tag("id,curriculum","curriculum",$filter);
		$this->load->view('templates/monitoring_template',$data);
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getScholars()
	{

		$data['scholars'] = $this->Crud_model->getScholars();
		$scholars['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['scholars'])
		{
			foreach($data['scholars'] as $key)
			{
				$middle_initial = substr($key->middle_name,0,1).'.';
				$full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
				$encrypt_id = encrypt($key->scholar_id);
				$encrypt_curriculum = encrypt($key->curriculum_id);
				$academic_year = $key->scholar_academic_year_from.'-'.$key->scholar_academic_year_to;
				$scholars['data']['data'][$id][] = $row;
				$scholars['data']['data'][$id][] = $key->student_number;
				$scholars['data']['data'][$id][] = $full_name;
				$scholars['data']['data'][$id][] = $key->curriculum;
				$scholars['data']['data'][$id][] = $key->section;
				$scholarships = $this->Crud_model->fetch_data("scholarship");
				$filter = array("scholar_id"=>$key->scholar_id);
				$latest_avg = $this->Crud_model->fetch_tag2("latest_avg","scholar_grades",$filter);
				$latest_avg = ($latest_avg=="" ? "N/A" : $latest_avg);
				$latest_avg2 = "";
				foreach($scholarships as $key2)
				{
					if($latest_avg != "N/A")
					{
						$color = $key2->color;
						if($latest_avg >= $key2->min_rating  && $latest_avg <= $key2->max_rating)
						{
							$latest_avg2 = '<span class="'.$color.'">'.$latest_avg.'</span>';
							break;
						}
						elseif($latest_avg < $key2->min_rating) //35.42 < 70
						{
							$latest_avg2 = '<span style="color:red;">'.$latest_avg.'</span>';
							break;
						}
					}
					else
					{
						$latest_avg2 = '<span>'.$latest_avg.'</span>';
					}
				}
				$scholars['data']['data'][$id][] = $latest_avg2;
				$scholars['data']['data'][$id][] = $key->scholar_type;
				$scholars['data']['data'][$id][] = $academic_year;
				$current_status = ($key->scholar_status==1 ? "Active":"Inactive");
				$scholars['data']['data'][$id][] = $current_status;
				if($key->scholar_status==1){
					$scholars['data']['data'][$id][] = '
					<td class="text-right">
						<a href="'.base_url('Monitoring/view/').$encrypt_id.'/'.$encrypt_curriculum.'" class="btn btn-simple btn-warning btn-icon ">
			              <i class="fa fa-eye"></i></a>
						 <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#scholarModal"><i class="fa fa-edit"></i></a>
			              <a href="#" class="btn btn-simple btn-danger btn-icon deactivate" data-id="'.$encrypt_id.'">
			              <i class="material-icons">close</i></a>
			          </td>';
				}
				else
				{
					$scholars['data']['data'][$id][] = '
					<td class="text-right">
							<a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#scholarModal"><i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'">
              <i class="material-icons">check</i></a>
          </td>';
				}
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($scholars['data']);
	}

	public function sort_scholar()
	{
		$curriculum = clean_data(post('curriculum'));
		if($curriculum!="")
		{
			$filter = array("a.curriculum_id"=>$curriculum);
			$data['scholars'] = $this->Crud_model->getScholars_sort($filter);
		}
		else
		{
			$data['scholars'] = $this->Crud_model->getScholars();
		}
		
		$scholars['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['scholars'])
		{
			foreach($data['scholars'] as $key)
			{
				$middle_initial = substr($key->middle_name,0,1).'.';
				$full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
				$encrypt_id = encrypt($key->scholar_id);
				$encrypt_curriculum = encrypt($key->curriculum_id);
				$academic_year = $key->scholar_academic_year_from.'-'.$key->scholar_academic_year_to;
				$scholars['data']['data'][$id][] = $row;
				$scholars['data']['data'][$id][] = $key->student_number;
				$scholars['data']['data'][$id][] = $full_name;
				$scholars['data']['data'][$id][] = $key->curriculum;
				$scholars['data']['data'][$id][] = $key->section;
				$scholarships = $this->Crud_model->fetch_data("scholarship");
				$filter = array("scholar_id"=>$key->scholar_id);
				$latest_avg = $this->Crud_model->fetch_tag2("latest_avg","scholar_grades",$filter);
				$latest_avg = ($latest_avg=="" ? "N/A" : $latest_avg);
				$latest_avg2 = "";
				foreach($scholarships as $key2)
				{
					if($latest_avg != "N/A")
					{
						$color = $key2->color;
						if($latest_avg >= $key2->min_rating  && $latest_avg <= $key2->max_rating)
						{
							$latest_avg2 = '<span class="'.$color.'">'.$latest_avg.'</span>';
							break;
						}
						elseif($latest_avg < $key2->min_rating) //35.42 < 70
						{
							$latest_avg2 = '<span style="color:red;">'.$latest_avg.'</span>';
							break;
						}
					}
					else
					{
						$latest_avg2 = '<span>'.$latest_avg.'</span>';
					}
				}
				$scholars['data']['data'][$id][] = $latest_avg2;
				$scholars['data']['data'][$id][] = $key->scholar_type;
				$scholars['data']['data'][$id][] = $academic_year;
				$current_status = ($key->scholar_status==1 ? "Active":"Inactive");
				$scholars['data']['data'][$id][] = $current_status;
				if($key->scholar_status==1){
					$scholars['data']['data'][$id][] = '
					<td class="text-right">
						<a href="'.base_url('Monitoring/view/').$encrypt_id.'/'.$encrypt_curriculum.'" class="btn btn-simple btn-warning btn-icon ">
			              <i class="fa fa-eye"></i></a>
						 <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#scholarModal"><i class="fa fa-edit"></i></a>
			              <a href="#" class="btn btn-simple btn-danger btn-icon deactivate" data-id="'.$encrypt_id.'">
			              <i class="material-icons">close</i></a>
			          </td>';
				}
				else
				{
					$scholars['data']['data'][$id][] = '
					<td class="text-right">
							<a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#scholarModal"><i class="fa fa-edit"></i></a>
              <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'">
              <i class="material-icons">check</i></a>
          </td>';
				}
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($scholars['data']);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$scholar_status = $this->Crud_model->fetch_tag_row("status","scholars",$filter);
		if($scholar_status)
		{
			$update_status = ($scholar_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('scholars',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}
	
	public function get_scholar()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('a.id'=>$id);
		$res['scholar'] = $this->Crud_model->getScholar($filter);
		$res["message"] = ($res["scholar"]?"success":"failed");
		echo json_encode($res);
	}

	public function view()
	{
		$scholar_id = decrypt(clean_data($this->uri->segment(3)));
		$curriculum_id = decrypt(clean_data($this->uri->segment(4)));
		$filter = array("a.id"=>$scholar_id);
		$data['scholar'] = $this->Crud_model->getScholar($filter);
		$filter = array("scholar_id"=>$scholar_id,"curriculum_id"=>$curriculum_id);
		$data["scholar_grades"] = $this->Crud_model->fetch_tag_row("*","scholar_grades",$filter);
		$filter = array("scholar_id"=>$scholar_id);
		$data["scholar_curriculum"] = $this->Crud_model->fetch("scholar_grades",$filter,"","","academic_year desc");
		$this->load->view('templates/monitoring_template',$data);
		
	}

	public function add()
	{
		if($_POST)
		{
			$scholar_id = decrypt(clean_data(post('scholar_id')));
			$curriculum_id = decrypt(clean_data(post('curriculum_id')));
			$subjects = clean_data(post('subjects'));
			$academic_year = clean_data(post('academic_year'));
			$section = clean_data(post('section'));
			$scholarship_type = clean_data(post('scholarship_type'));
			$grades_arr = array();
			foreach(post('grades') as $value)
			{
				array_push($grades_arr, clean_data($value));
			}
			$grades = implode(";",$grades_arr);
			$latest_avg = end($grades_arr);
			
			$filter = array("scholar_id"=>$scholar_id,"curriculum_id"=>$curriculum_id);
			$check_exists = $this->Crud_model->check_exist("scholar_grades",$filter);
			if($check_exists)
			{
				$data = array("grades"=>$grades,"latest_avg"=>$latest_avg);
				$this->Crud_model->update("scholar_grades",$data,$filter);
			}
			else
			{
				$data = array("scholar_id"=>$scholar_id,"curriculum_id"=>$curriculum_id,
				"subjects"=>$subjects,"grades"=>$grades,"academic_year"=>$academic_year,"section"=>$section,
				"scholar_type"=>$scholarship_type,"latest_avg"=>$latest_avg);
				$this->Crud_model->insert("scholar_grades",$data);

			}
			//$res["message"] = "success";
			//echo json_encode($res);		
			redirect(base_url('Monitoring/view/').post('scholar_id').'/'.post('curriculum_id'));
		}
		else
		{
			$this->load->view('templates/monitoring_template');
		}
	}

	public function final_submit()
	{
		if($_POST)
		{
			$scholar_id = decrypt(clean_data(post('scholar_id')));
			$curriculum_id = decrypt(clean_data(post('curriculum_id')));
			$subjects = clean_data(post('subjects'));
			$academic_year = clean_data(post('academic_year'));
			$section = clean_data(post('section'));
			$scholarship_type = clean_data(post('scholarship_type'));
			$grades_arr = array();
			foreach(post('grades') as $value)
			{
				array_push($grades_arr, clean_data($value));
			}
			$grades = implode(";",$grades_arr);
			$latest_avg = end($grades_arr);
			
			$filter = array("scholar_id"=>$scholar_id,"curriculum_id"=>$curriculum_id);
			$check_exists = $this->Crud_model->check_exist("scholar_grades",$filter);
			if($check_exists)
			{
				$data = array("status"=>0,"latest_avg"=>$latest_avg);
				$this->Crud_model->update("scholar_grades",$data,$filter);
				$res["message"] = "success";
			}
			else
			{
				$res["message"] = "failed";
			}
			
			echo json_encode($res);		
			//redirect(base_url('Monitoring/view/').post('scholar_id').'/'.post('curriculum_id'));
		}
		else
		{
			$this->load->view('templates/monitoring_template');
		}
	}

	function update_final_submit()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$scholar_status = $this->Crud_model->fetch_tag_row("status","scholar_grades",$filter);
		if($scholar_status)
		{
			$update_status = ($scholar_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('scholar_grades',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}

	public function update_scholar()
	{
		if($_POST)
		{
			$id = decrypt(clean_data(post('id')));
			$student_number = clean_data(post('student_number'));
			$scholarship_type = clean_data(post('scholarship_type'));
			$created_at = clean_data(post('created_at'));
			$academic_year_from = clean_data(post('academic_year_from'));
			$academic_year_to = clean_data(post('academic_year_to'));
			$curriculum_id = clean_data(post('curriculum_id'));
			$section = ucwords(clean_data(post('section')));
			$contact_number = clean_data(post('contact_number'));
			
			$data = array("student_number"=>$student_number,"scholarship_type"=>$scholarship_type,
			"created_at"=>$created_at,"academic_year_from"=>$academic_year_from,"academic_year_to"=>$academic_year_to,
			"curriculum_id"=>$curriculum_id,"contact_number"=>$contact_number,"section"=>$section);
			$filter = array("id"=>$id);
			$query_status = $this->Crud_model->update('scholars',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	public function add_scholar()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$school = clean_data("St. Joseph School"); 	
			$contact_number = "";
			$academic_year_from = clean_data(post('academic_year_from'));
			$academic_year_to = clean_data(post('academic_year_to'));
			$exam_date = clean_data(post('date'));
			$rating = "0";

			$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"school"=>$school,
				"rating"=>$rating,"exam_date"=>$exam_date,"academic_year_to"=>$academic_year_to,
				"contact_number"=>$contact_number,"academic_year_from"=>$academic_year_from);
			$result = $this->Crud_model->insert_id("examinations",$data);

			if($result)
			{
				$student_number = clean_data(post('student_number'));
				$scholarship_type = clean_data(post('scholarship_type'));
				$examinations_id = $result;
				$created_at = clean_data(post('created_at'));
				$curriculum_id = clean_data(post('curriculum_id'));
				$section = ucwords(clean_data(post('section')));
				$data = array("student_number"=>$student_number,"scholarship_type"=>$scholarship_type,
				"created_at"=>$created_at,"academic_year_from"=>$academic_year_from,"academic_year_to"=>$academic_year_to,
				"curriculum_id"=>$curriculum_id,"contact_number"=>$contact_number,"section"=>$section,
				"examinations_id"=>$examinations_id);
				$this->Crud_model->insert("scholars",$data);
				$res["message"] = 'success';

			}
			else
			{
				$res["message"] = 'failed';
			}
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	public function print_active_scholars()
	{
		$curriculum = clean_data(post('curriculum'));
		$academic_year_from = clean_data(post('academic_year_from'));
		$academic_year_to = clean_data(post('academic_year_to'));
		$data["academic_year"] = $academic_year_from.' - '.$academic_year_to;
		if($curriculum=="")
			$filter = array("a.academic_year_from"=>$academic_year_from,"a.academic_year_to"=>$academic_year_to);
		else
			$filter = array("a.academic_year_from"=>$academic_year_from,"a.academic_year_to"=>$academic_year_to,"a.curriculum_id"=>$curriculum);
		$data['scholars'] = $this->Crud_model->print_scholars($filter);
		$this->load->view('templates/print_scholars',$data);
	}

	public function print_grade()
	{
		$user_id = decrypt($this->session->id);
		$filter = array("id"=>$user_id);
		$data['user_details'] = $this->Crud_model->fetch_tag_row("*","users",$filter);

		$scholar_id = decrypt(clean_data(post('scholar_id')));
		$curriculum_id = decrypt(clean_data(post('curriculum_id')));
		$filter = array("a.id"=>$scholar_id);
		$data['scholar'] = $this->Crud_model->getScholar($filter);
		$filter = array("scholar_id"=>$scholar_id,"curriculum_id"=>$curriculum_id);
		$data["scholar_grades"] = $this->Crud_model->fetch_tag_row("*","scholar_grades",$filter);
		$filter = array("scholar_id"=>$scholar_id);
		$data["scholar_curriculum"] = $this->Crud_model->fetch("scholar_grades",$filter,"","","academic_year desc");
		$this->load->view('templates/print_grade',$data);
	}
	
}