<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/users_template');
	}

	public function add()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$school = ucwords(clean_data(post('school')));
			$contact_number = clean_data(post('contact_number'));
			$rating = clean_data(post('rating'));
			$exam_date = clean_data(post('date'));
			$academic_year_from = clean_data(post('academic_year_from'));
			$academic_year_to = clean_data(post('academic_year_to'));

			$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"school"=>$school,
				"rating"=>$rating,"exam_date"=>$exam_date,"academic_year_to"=>$academic_year_to,
				"contact_number"=>$contact_number,"academic_year_from"=>$academic_year_from);
			$result = $this->Crud_model->insert("examinations",$data);
			$res["message"]= ($result ? "success" : "failed");
			echo json_encode($res);
		}
		else
		{
			$this->load->view('templates/exams_template');
		}
	}

	public function records()
	{
		
		$this->load->view('templates/exams_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getExams()
	{
		$order_by = "exam_date desc";
		$filter = array("status <"=>2);
		$data['exams'] = $this->Crud_model->fetch_data("examinations",$filter,"",$order_by);
		$exams['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['exams'])
		{
			foreach($data['exams'] as $key)
			{
				$middle_initial = substr($key->middle_name,0,1).'.';
				$full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
				$encrypt_id = encrypt($key->id);
				$encrypt_yearto = encrypt($key->academic_year_to);
				$encrypt_yearfrom = encrypt($key->academic_year_from);
				$scholarship = "Not Qualified";
				$scholarships = $this->Crud_model->fetch_data("scholarship");
				$scholar_id = "";
				foreach($scholarships as $key2)
				{
					$color = $key2->color;
					if($key->rating >= $key2->min_rating  && $key->rating <= $key2->max_rating)
					{
						$scholarship = '<span class="'.$color.'">'.$key2->scholar_type.'</span>';
						$scholar_id = encrypt($key2->id);
						break;
					}
				}
				$academic_year = $key->academic_year_from.'-'.$key->academic_year_to;
				$exams['data']['data'][$id][] = $row;
				$exams['data']['data'][$id][] = $full_name;
				$exams['data']['data'][$id][] = $key->school;
				$exams['data']['data'][$id][] = $key->contact_number;
				$exams['data']['data'][$id][] = $key->rating;
				$exams['data']['data'][$id][] = $scholarship;
				$exams['data']['data'][$id][] = $key->exam_date;
				$exams['data']['data'][$id][] = $academic_year;
				if($key->status==1){
					$exams['data']['data'][$id][] = '
					<td class="text-right">
						<p>None</p>
	                </td>';
				}
				else
				{
					$exams['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#examModal"><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'" data-type="'.$scholar_id.'" data-yearto='.$encrypt_yearto.' data-yearfrom='.$encrypt_yearfrom.'>
	                    <i class="material-icons">add</i></a>
	                </td>';
				}
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($exams['data']);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$type = decrypt(clean_data(post('type')));
		$academic_year_from = decrypt(clean_data(post('yearfrom')));
		$academic_year_to = decrypt(clean_data(post('yearto')));
		$filter = array('id'=>$id);
		$exam_status = $this->Crud_model->fetch_tag_row("status","examinations",$filter);
		if($exam_status)
		{
			$update_status = ($exam_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('examinations',$data,$filter);
			$data= array("examinations_id"=>$id,"scholarship_type"=>$type,"academic_year_from"=>$academic_year_from,"academic_year_to"=>$academic_year_to);
			$this->Crud_model->insert("scholars",$data);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}
	
	public function get_exam()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['exam'] = $this->Crud_model->fetch_tag_row("*","examinations",$filter);
		$res["message"] = ($res["exam"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_exam()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$school = ucwords(clean_data(post('school')));
			$contact_number = clean_data(post('contact_number'));
			$rating = clean_data(post('rating'));
			$academic_year_from = clean_data(post('academic_year_from'));
			$academic_year_to = clean_data(post('academic_year_to'));
			$date = clean_data(post('date'));
			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			
			$data = array("first_name"=>$first_name,"last_name"=>$last_name,
			"middle_name"=>$middle_name,"suffix"=>$suffix,"school"=>$school,
			"rating"=>$rating,"contact_number"=>$contact_number,"exam_date"=>$date,"academic_year_from"=>$academic_year_from
			,"academic_year_to"=>$academic_year_to);
			
			$query_status = $this->Crud_model->update('examinations',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	public function print_exam_results()
	{
		$academic_year_from = clean_data(post('academic_year_from'));
		$academic_year_to = clean_data(post('academic_year_to'));
		$data["academic_year"] = $academic_year_from.' - '.$academic_year_to;
		$filter = array("academic_year_from"=>$academic_year_from,"academic_year_to"=>$academic_year_to,
			"status <"=>2);
		$order_by = "school desc";
		$data['exams'] = $this->Crud_model->fetch_data("examinations",$filter,"","","school asc");
		$data['scholarships'] = $this->Crud_model->fetch_data("scholarship");
		$this->load->view('templates/print_scholarship_result',$data);
	}
	
}