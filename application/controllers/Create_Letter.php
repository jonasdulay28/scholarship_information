<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Create_Letter extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$data["schools"] = $this->Crud_model->get_exam_schools();
		$this->load->view('templates/create_letter_template',$data);
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}
	
	public function print_letter()
	{
		$data['title'] = clean_data(post('title'));
		$data['full_name'] = clean_data(post('full_name'));
		$data['designation'] = clean_data(post('designation'));
		$data['school'] = clean_data(post('school'));
		$data['exam_school'] = clean_data(post('exam_school'));
		$data["content"] = post('content');
		$data['address'] = clean_data(post('address'));
		$data['academic_year_from']= clean_data(post('academic_year_from'));
		$data['academic_year_to']= clean_data(post('academic_year_to'));
		$filter = array("academic_year_from"=>$data['academic_year_from'],"academic_year_to"=>$data['academic_year_to']);
		$data["exams"] = $this->Crud_model->get_examination_results($data['exam_school'],$filter);
		$data['scholarships'] = $this->Crud_model->fetch_data("scholarship");
		$data['sincerely_name'] = clean_data(post('sincerely_name'));
		$data['sincerely_designation'] = clean_data(post('sincerely_designation'));
		$data['noted_by_name'] = clean_data(post('noted_by_name'));
		$data['noted_by_designation'] = clean_data(post('noted_by_designation'));
		$data['noted_by_name2'] = clean_data(post('noted_by_name2'));
		$data['noted_by_designation2'] = clean_data(post('noted_by_designation2'));
		$this->load->view('templates/print_exam_results',$data);
	}
}