<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scholarship extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
	}

	public function index()
	{
		
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function scholarship()
	{
		$this->load->view('templates/scholarship_template');
	}

	public function getScholarships()
	{
		$order_by = "id asc";
		$data['scholarship'] = $this->Crud_model->fetch_data("scholarship","","",$order_by);
		$scholarship['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['scholarship'])
		{
			foreach($data['scholarship'] as $key)
			{
				$current_status = ($key->status==1 ? "Active":"Inactive");
				$status = ($key->status==1 ? "Deactivate":"Activate");
				$encrypt_id = encrypt($key->id);
				$scholarship['data']['data'][$id][] = $row;
				$scholarship['data']['data'][$id][] = $key->scholar_type;
				$scholarship['data']['data'][$id][] = $key->min_rating.' - '.$key->max_rating;
				$scholarship['data']['data'][$id][] = $current_status;
				if($key->status==1){
					$scholarship['data']['data'][$id][] = '
					<td class="text-center">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-toggle="modal" data-target="#scholarshipModal" data-id="'.$encrypt_id.'"><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple btn-danger btn-icon deactivate" data-id="'.$encrypt_id.'">
	                    <i class="material-icons">close</i></a>
	                </td>';
				}
				else
				{
					$scholarship['data']['data'][$id][] = '
					<td class="text-center">
	                    <a href="#" class="btn btn-simple btn-icon modify" data-toggle="modal" data-target="#scholarshipModal"><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'">
	                    <i class="material-icons">check</i></a>
	                </td>';
				}
				
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($scholarship['data']);
	}

	function update_status_scholarship()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$scholarship_status = $this->Crud_model->fetch_tag_row("status","scholarship",$filter);
		if($scholarship_status)
		{
			$update_status = ($scholarship_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('scholarship',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}

	public function get_scholarship()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['scholarship'] = $this->Crud_model->fetch_tag_row("*","scholarship",$filter);
		$res["message"] = ($res["scholarship"]?"success":"failed");
		echo json_encode($res);
	}
	
	public function update_scholarship()
	{
		if($_POST)
		{
			$scholar_type = ucwords(clean_data(post('scholarship')));
			$min_rating= clean_data(post('min_rating'));
			$max_rating = clean_data(post('max_rating'));
			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			$data = array('scholar_type'=>$scholar_type,'min_rating'=>$min_rating
				,'max_rating'=>$max_rating);
			$query_status = $this->Crud_model->update('scholarship',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}

	
}