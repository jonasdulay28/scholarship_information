<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pabile_Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Crud_model');
		if(!isset($this->session->branch_id))
		{
			session_destroy();
			redirect(base_url());
		}
		if($this->check_user()==false)
		{
			session_destroy();
			redirect(base_url());
		}
	}

	function index()
	{
		$data["user_roles"] = $this->Crud_model->fetch_tag_array("id,role_name","user_roles","","","","id ASC");
		$data["branches"] = $this->Crud_model->fetch_tag_array("id,branch_name","branches");
		$ip_add = $this->input->ip_address();
		$this->load->view('templates/admin_template',$data);
	}

	public function get_users()
	{
		$order_by = "role_id desc";
		$data['users'] = $this->Crud_model->get_users();
		$users['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['users'])
		{
			foreach($data['users'] as $key)
			{
				$users['data']['data'][$id][] = $row;
				$users['data']['data'][$id][] = $key->name;
				$users['data']['data'][$id][] = $key->email;
				$users['data']['data'][$id][] = $key->contact_number;
				$users['data']['data'][$id][] = $key->address;
				$users['data']['data'][$id][] = $key->role_name;
				$users['data']['data'][$id][] = $key->branch_name;
				$current_status = ($key->status==1 ? "Active":"Inactive");
				$users['data']['data'][$id][] = $current_status;
				$status = ($key->status==1 ? "Deactivate":"Activate");
				$users['data']['data'][$id][] = '
				<div class="btn-group">
					   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					     Action <span class="caret"></span>
					   </button>
					   <ul class="dropdown-menu">
					   		<li><a href="#update_user_modal"  data-toggle="modal" class="update_user_btn" data-id="'.encrypt($key->oauth_uid).'">Update</a></li>
					   		<li role="separator" class="divider"></li>
					   		<li><a href="javascript:void(0);" name="update_status" class="update_status" data-id="'.encrypt($key->oauth_uid).'">
					   		'.$status.'</a></li>
					   </ul> 
				  </div>';
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($users['data']);
	}

	public function get_user()
	{
		$res = array("message"=>"","token"=>"","error"=>"","user");
		if($_POST)
		{
			$oauth_uid = decrypt(clean_data($this->input->post("oauth_uid")));
			$filter = array("a.oauth_uid"=>$oauth_uid);
			$res['user'] = $this->Crud_model->get_user($filter);
			$res['user']->role_id = encrypt($res['user']->role_id); 
			$res['user']->branch_id = encrypt($res['user']->branch_id); 
			$res['token'] = $this->security->get_csrf_hash();
		}else
		{
			$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>Invalid Request</h6></div></div>';
			$res['token'] = $this->security->get_csrf_hash();
			$res["error"]="exist";
		}
		
		
		echo json_encode($res);
	}

	public function add_user()
	{
		if(!$this->input->is_ajax_request()){ exit("no valid req."); }
		$res = array("message"=>"","token"=>"","error"=>"");
		if($_POST)
		{
			$FormRules = array(
						array(
								'field' => 'name',
								'label' =>'Full name',
								'rules'=>'required|trim|xss_clean|max_length[80]|min_length[2]'
							),
						array(
								'field' => 'address',
								'label' =>'Address',
								'rules'=>'required|trim|xss_clean|max_length[255]|min_length[2]'
							),
						array(
								'field' => 'contact_num',
								'label' =>'Contact number',
								'rules'=>'required|trim|xss_clean|exact_length[10]'
							),

						array(
								'field' => 'email',
								'label' =>'Email',
								'rules'=>'required|trim|xss_clean|max_length[60]|valid_email|is_unique[users.email]|min_length[12]'
							),
						array(
								'field' => 'role',
								'label' =>'Role',
								'rules'=>'required|trim|xss_clean'
							),
						array(
								'field' => 'branch',
								'label' =>'Branch',
								'rules'=>'required|trim|xss_clean'
							)
					);
				$this->form_validation->set_rules($FormRules);
				if($this->form_validation->run()==TRUE)
				{
					$name = ucwords(clean_data($this->input->post('name')));
					$address= clean_data($this->input->post('address'));
					$password = hash_password("123Qwe1!");
					$contact = clean_data($this->input->post('contact_num'));
					
					$email = strtolower(clean_data($this->input->post('email')));
					$role = decrypt(clean_data($this->input->post('role')));
					$branch = decrypt(clean_data($this->input->post('branch')));
					$oauth_uid="";
					do
					{
						//regenerate id if exists
						$oauth_uid = rand(1000000,9999999);
						$filter=array("oauth_uid"=>$oauth_uid);
						$check_exist= $this->Crud_model->check_exist("users",$filter);
					}while($check_exist > 0);
					//check role exist
					$data = array('name'=>$name,'address'=>$address,'branch_id'=>$branch,'oauth_uid'=>$oauth_uid,
						'oauth_provider'=>'pabile','status'=>1,'password'=>$password,'contact_number'=>$contact,
						'role_id'=>$role,'email'=>$email);
					$query_status = $this->Crud_model->insert('users',$data);
					$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>User updated successfully</h6></div></div>';
					$res['token'] = $this->security->get_csrf_hash();
					$res["error"]="";

					//Audit Trail

					//End of Audit Trail
				}
				else
				{
					$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>'.validation_errors().'</h6></div></div>';
					$res['token'] = $this->security->get_csrf_hash();
					$res["error"]="exist";
				}

		}
		else
		{
			$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>Invalid Request</h6></div></div>';
			$res['token'] = $this->security->get_csrf_hash();
			$res["error"]="exist";
		}
		echo json_encode($res);
	}

	public function update_user()
	{
		if(!$this->input->is_ajax_request()){ exit("no valid req."); }
		$res = array("message"=>"","token"=>"","error"=>"");
		if($_POST)
		{
			$FormRules = array(
						array(
								'field' => 'name',
								'label' =>'Full name',
								'rules'=>'required|trim|xss_clean|max_length[80]|min_length[2]'
							),
						array(
								'field' => 'address',
								'label' =>'Address',
								'rules'=>'required|trim|xss_clean|max_length[255]|min_length[2]'
							),
						array(
								'field' => 'contact_num',
								'label' =>'Contact number',
								'rules'=>'required|trim|xss_clean|exact_length[10]'
							),

						array(
								'field' => 'role',
								'label' =>'Role',
								'rules'=>'required|trim|xss_clean'
							),
						array(
								'field' => 'branch',
								'label' =>'Branch',
								'rules'=>'required|trim|xss_clean'
							)

					);
				$this->form_validation->set_rules($FormRules);
				if($this->form_validation->run()==TRUE)
				{
					$name = ucwords(clean_data($this->input->post('name')));
					$address= clean_data($this->input->post('address'));
					$contact = clean_data($this->input->post('contact_num'));
					$role = decrypt(clean_data($this->input->post('role')));
					$branch = decrypt(clean_data($this->input->post('branch')));
					$oauth_uid = decrypt(clean_data($this->input->post('oauth_uid')));
					$filter = array('oauth_uid'=>$oauth_uid);
					$data = array('name'=>$name,'address'=>$address
						,'contact_number'=>$contact,'role_id'=>$role,'branch_id'=>$branch);
					$this->Crud_model->update('users',$data,$filter);
					$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>New User added successfully</h6></div></div>';
					$res['token'] = $this->security->get_csrf_hash();
					$res["error"]="";

					//Audit Trail
					//End of Audit Trail
				}
				else
				{
					$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>'.validation_errors().'</h6></div></div>';
					$res['token'] = $this->security->get_csrf_hash();
					$res["error"]="exists";
				}
			
		}
		else
		{
			$res["message"] = '<div class="panel panel-danger"><div class="panel-body" style="color:red;"><h6>Invalid Request</h6></div></div>';
			$res['token'] = $this->security->get_csrf_hash();
			$res["error"]="exists";
		}
		echo json_encode($res);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$oauth_uid = decrypt(clean_data($this->input->post('oauth_uid')));
		$filter = array('oauth_uid'=>$oauth_uid);
		$user_status = $this->Crud_model->fetch_tag_row("status","users",$filter);
		if($user_status)
		{
			$update_status = ($user_status->status==1 ? 10 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('users',$data,$filter);
			$res['token'] = $this->security->get_csrf_hash();
			$res["error"]="";
		}
		else
		{

			$res['token'] = $this->security->get_csrf_hash();
			$res["error"]="exists";
		}
		
		echo json_encode($res);
	}

	public function check_user()
	{
		
		if(empty($this->session->id) && !isset($this->session->id))
		{
			return false;
		}
		else
		{
			//check if deactivated or not
			$filter = array("oauth_uid"=>decrypt($this->session->id),"status > "=>0,"status < "=>10,"role_id > "=>1);
			$user_status = $this->Crud_model->check_exist("users",$filter);
			if($user_status)
				return true;
			else
				return false;
		}
	}
}