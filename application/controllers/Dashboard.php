<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$filter = array("a.status"=>1);
		$scholars = $this->Crud_model->get_scholar_chart($filter);
		$scholar_count = array();
		foreach ($scholars as $key) {
			$scholar_count[] = array(
			  'label'  => $key->scholar_type,
			  'value'  => $key->count_scholar
			 );
		}
		$data['top_10_scholars'] = $this->Crud_model->get_top10_scholars();
		$data['scholar_count'] = json_encode($scholar_count);
		$data["num_users"] = $this->Crud_model->count_records("users");
		$data["num_exams"] = $this->Crud_model->count_records("examinations");
		$data["num_curriculum"] = $this->Crud_model->count_records("curriculum");
		$data["num_scholars"] = $this->Crud_model->count_records("scholars");
		$this->load->view('templates/dashboard_template',$data);
	}

	public function add()
	{
		if($_POST)
		{

		}
		else
		{
			$this->load->view('templates/dashboard_template');
		}
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function get_scholar_chart()
	{
		/*$ye = array("label"=>"wew");
		$ste = array_merge($ye, array("label" => "shit"));
		$donut_chart = array($ste);
		$filter = array("status"=>1);
		$scholars = $this->Crud_model->get_scholar_chart($filter);
		foreach ($scholars as $key) {
			echo $key->scholarship_type;
		}
		print_r($scholars);*/

		$filter = array("a.status"=>1);
		$scholars = $this->Crud_model->get_scholar_chart($filter);
		$data = array();
		foreach ($scholars as $key) {
			$data[] = array(
			  'label'  => $key->scholar_type,
			  'value'  => $key->count_scholar
			 );
		}
		
		echo json_encode($data);
	}
	
}