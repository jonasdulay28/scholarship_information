<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		session_destroy();
		$res["url"] = base_url();
		echo json_encode($res);
	}	
}