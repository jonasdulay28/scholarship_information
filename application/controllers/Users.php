<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/users_template');
	}

	public function add()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$address = ucwords(clean_data(post('address')));
			$contact_number = clean_data(post('contact_number'));
			$email_address = strtolower(clean_data(post('email_address')));
			$password = hash_password(clean_data(post('password')));
			$role = clean_data(post('role'));
			$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"address"=>$address,
				"email_address"=>$email_address,"role"=>$role,
				"contact_number"=>$contact_number,"password"=>$password);
			$result = $this->Crud_model->insert("users",$data);
			$res["message"]= ($result ? "success" : "failed");
			echo json_encode($res);
		}
		else
		{
			$this->load->view('templates/users_template');
		}
	}

	public function records()
	{
		
		$this->load->view('templates/users_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getRecords()
	{
		$order_by = "role asc";
		$data['users'] = $this->Crud_model->fetch_data("users","","",$order_by);
		$users['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['users'])
		{
			foreach($data['users'] as $key)
			{
				$middle_initial = substr($key->middle_name,0,1).'.';
				$full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
				$current_status = ($key->status==1 ? "Active":"Inactive");
				$status = ($key->status==1 ? "Deactivate":"Activate");
				$role = ($key->role==1?"Administrator":($key->role==2?"Encoder":"Student"));
				$encrypt_id = encrypt($key->id);
				$users['data']['data'][$id][] = $row;
				$users['data']['data'][$id][] = $full_name;
				$users['data']['data'][$id][] = $key->email_address;
				$users['data']['data'][$id][] = $key->address;
				$users['data']['data'][$id][] = $key->contact_number;
				$users['data']['data'][$id][] = $role;
				$users['data']['data'][$id][] = $current_status;
				if($key->status==1){
					$users['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#userModal" ><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple btn-danger btn-icon deactivate" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#userModal">
	                    <i class="material-icons">close</i></a>
	                </td>';
				}
				else
				{
					$users['data']['data'][$id][] = '
					<td class="text-right">
	                    <a href="#" class="btn btn-simple  btn-icon modify" data-id="'.$encrypt_id.'" data-toggle="modal" data-target="#userModal"><i class="fa fa-edit"></i></a>
	                    <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'">
	                    <i class="material-icons">check</i></a>
	                </td>';
				}
				


				/*$users['data']['data'][$id][] = '
				<div class="btn-group">
					   <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					     Action <span class="caret"></span>
					   </button>
					   <ul class="dropdown-menu">
					   		<li><a href="#update_user_modal"  data-toggle="modal" class="update_user_btn" data-id="'.encrypt($key->id).'">Update</a></li>
					   		<li role="separator" class="divider"></li>
					   		<li><a href="javascript:void(0);" name="update_status" class="update_status" data-id="'.encrypt($key->id).'">
					   		'.$status.'</a></li>
					   </ul> 
				  </div>';*/
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($users['data']);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$user_status = $this->Crud_model->fetch_tag_row("status","users",$filter);
		if($user_status)
		{
			$update_status = ($user_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('users',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}
	
	public function get_user()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['user'] = $this->Crud_model->fetch_tag_row("*","users",$filter);
		$res["message"] = ($res["user"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_user()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$address = ucwords(clean_data(post('address')));
			$contact_number = clean_data(post('contact_number'));
			$email_address = strtolower(clean_data(post('email_address')));
			$password = hash_password(clean_data(post('password')));
			$role = clean_data(post('role'));
			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			if(!empty(post('password')))
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"address"=>$address,
				"role"=>$role,"contact_number"=>$contact_number,"password"=>$password);
			}
			else
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"address"=>$address,
				"role"=>$role,"contact_number"=>$contact_number);
			}
			
			$query_status = $this->Crud_model->update('users',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}
	
}