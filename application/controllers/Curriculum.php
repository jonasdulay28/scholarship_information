<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curriculum extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/curriculum_template');
	}

	public function add()
	{
		if($_POST)
		{
			$curriculum = ucwords(clean_data(post('curriculum')));
			$description = clean_data(post('description'));
			$subjects_arr = array();
			foreach(post('subject') as $value)
			{
				array_push($subjects_arr, str_replace(";", "", clean_data($value)));
			}
			$subjects = implode(";",$subjects_arr);

			$data = array("curriculum"=>$curriculum,"description"=>$description,
				"subjects"=>$subjects);
			$result = $this->Crud_model->insert("curriculum",$data);
			$res["message"]= ($result ? "success" : "failed");
			echo json_encode($res);
		}
		else
		{
			$this->load->view('templates/curriculum_template');
		}
	}

	public function records()
	{
		
		$this->load->view('templates/curriculum_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getCurriculum()
	{
		$order_by = "exam_date desc";
		$data['curriculum'] = $this->Crud_model->fetch_data("curriculum","","",$order_by);
		$curriculum['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['curriculum'])
		{
			foreach($data['curriculum'] as $key)
			{
				$encrypt_id = encrypt($key->id);
				$current_status = ($key->status==1 ? "Active":"Inactive");
				$curriculum['data']['data'][$id][] = $row;
				$curriculum['data']['data'][$id][] = $key->curriculum;
				$curriculum['data']['data'][$id][] = $key->description;
				$curriculum['data']['data'][$id][] = str_replace(";", ", ", $key->subjects);
				$curriculum['data']['data'][$id][] = date('m-d-Y',strtotime($key->created_at));
				$curriculum['data']['data'][$id][] = $current_status;
				if($key->status==1){
					$curriculum['data']['data'][$id][] = '
					<td class="text-right">
              <a href="#" class="btn btn-simple btn-danger btn-icon deactivate" data-id="'.$encrypt_id.'" >
              <i class="material-icons">close</i></a>
          </td>';
				}
				else
				{
					$curriculum['data']['data'][$id][] = '
					<td class="text-right">
              <a href="#" class="btn btn-simple btn-success btn-icon activate" data-id="'.$encrypt_id.'">
              <i class="material-icons">check</i></a>
          </td>';
				}
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($curriculum['data']);
	}

	function update_status()
	{
		$res = array("message"=>"","token"=>"","error"=>"");
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$curriculum_status = $this->Crud_model->fetch_tag_row("status","curriculum",$filter);
		if($curriculum_status)
		{
			$update_status = ($curriculum_status->status==1 ? 0 : 1);
			$data = array('status'=>$update_status);
			$this->Crud_model->update('curriculum',$data,$filter);
			$res["message"]="success";
		}
		else
		{
			$res["message"]="failed";
		}
		
		echo json_encode($res);
	}
	
	public function get_exam()
	{
		$id = decrypt(clean_data(post('id')));
		$filter = array('id'=>$id);
		$res['exam'] = $this->Crud_model->fetch_tag_row("*","examinations",$filter);
		$res["message"] = ($res["exam"]?"success":"failed");
		echo json_encode($res);
	}

	public function update_user()
	{
		if($_POST)
		{
			$first_name = ucwords(clean_data(post('first_name')));
			$last_name = ucwords(clean_data(post('last_name')));
			$middle_name = ucwords(clean_data(post('middle_name')));
			$suffix = ucwords(clean_data(post('suffix')));
			$address = ucwords(clean_data(post('address')));
			$contact_number = clean_data(post('contact_number'));
			$email_address = strtolower(clean_data(post('email_address')));
			$password = hash_password(clean_data(post('password')));
			$role = clean_data(post('role'));
			$id = decrypt(clean_data(post('id')));
			$filter = array('id'=>$id);
			if(!empty(post('password')))
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"address"=>$address,
				"role"=>$role,"contact_number"=>$contact_number,"password"=>$password);
			}
			else
			{
				$data = array("first_name"=>$first_name,"last_name"=>$last_name,
				"middle_name"=>$middle_name,"suffix"=>$suffix,"address"=>$address,
				"role"=>$role,"contact_number"=>$contact_number);
			}
			
			$query_status = $this->Crud_model->update('users',$data,$filter);
			$res["message"] = ($query_status?"success":"failed");
		}
		else
		{
			$res["message"] = 'failed';
		}
		echo json_encode($res);
	}
	
}