<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
		else
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('templates/dashboard_template');
	}

	public function add()
	{
		if($_POST)
		{

		}
		else
		{
			$this->load->view('templates/dashboard_template');
		}
	}

	public function audit_trails()
	{
		$this->load->view('templates/audit_template');
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function getAudit()
	{
		$order_by = "created_at desc";
		$data['audit'] = $this->Crud_model->get_audit_trails();
		$audit['data']['data']=array();
		$id=0;
		$row = 1;
		if($data['audit'])
		{
			foreach($data['audit'] as $key)
			{
				$encrypt_id = encrypt($key->id);
				
				$audit['data']['data'][$id][] = $row;
				$audit['data']['data'][$id][] = $key->email;
				$audit['data']['data'][$id][] = $key->description;
				$audit['data']['data'][$id][] = $key->created;
				$id++;
				$row++;
				
			}
		}
		
		echo json_encode($audit['data']);
	}
	

	
}