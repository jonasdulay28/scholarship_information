<!doctype html>
<html lang="en">
<head>
    <?php 
        $segment1 = strtolower($this->uri->segment(1));
        $segment2 = strtolower($this->uri->segment(2));
        $segments_arr['segment1']= $segment1;
     ?>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/img/logo.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/logo.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>St. Joseph School SPC</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url()?>material/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url()?>material/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="<?php echo base_url()?>material/assets/css/demo.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/sweetalert2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/styles.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href='<?php echo base_url()?>assets/iconfont/material-icons.css' rel='stylesheet' type='text/css'>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url()?>material/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>material/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>material/assets/js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/global.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-beta.1/classic/ckeditor.js"></script>
    

    <style>
    .ck-editor__editable {
        min-height: 300px;
    }
    </style>
</head>

<body>
    <div class="wrapper">
        <?php $this->load->view('includes/sidebar',$segments_arr); ?>
        <div class="main-panel">
            <?php 
                $this->load->view('includes/navbar',$segments_arr); 
                switch ($segment1) {
                    case 'create_letter':
                        if($segment2=="" || $segment2=="index")
                            $this->load->view('pages/create_letter');
                        elseif($segment2=="print")
                            $this->load->view('templates/print_exam_results');
                        break;
                    default:
                        echo $segment2;
                        break;
                }
                $this->load->view('includes/footer'); 
            ?>
            
        </div>
    </div>
    <script src="<?php echo base_url()?>assets/js/sweetalert2.min.js"></script>
    <!--  Charts Plugin 
    <script src="<?php echo base_url()?>material/assets/js/chartist.min.js"></script>-->
    <!--  Dynamic Elements plugin -->
    <script src="<?php echo base_url()?>material/assets/js/arrive.min.js"></script>
    <!--  PerfectScrollbar Library -->
    <script src="<?php echo base_url()?>material/assets/js/perfect-scrollbar.jquery.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url()?>material/assets/js/bootstrap-notify.js"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="<?php echo base_url()?>material/assets/js/material-dashboard.js?v=1.2.0"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="<?php echo base_url()?>material/assets/js/demo.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.datatables.js"></script>
    <script type="text/javascript">
        /*$(document).ready(function() {

            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });*/
    </script>
</body>


</html>