<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/img/logo.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/logo.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>St. Joseph School SPC</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url()?>material/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>material/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/styles.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href='<?php echo base_url()?>assets/iconfont/material-icons.css' rel='stylesheet' type='text/css'>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url()?>material/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>material/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/global.js"></script>
    <style type="text/css">
    body
    {
        background-color: white;
    }
    table
    {
        width: 100%;
    }
    .st_joseph_text{
        font-family: Old-English;
        font-size: 18px;
        text-align: center;
    }
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-yw4l{vertical-align:top}
    @media print {
      @page { margin: 0; }
      body { margin: 1.6cm; }
    }
    @page { margin: 0; }
    .sub_text{
        text-align: center;
        font-size: 16px;
    }
    .title_text{
        text-align: center;
        font-size: 16px;
    }
    .date{
        font-size: 16px;
        margin-left: 20px;
    }
    th
    {
        font-weight: bold;
    }
    td
    {
        text-align: center;
    }
    .gray
    {
        background-color:#dedede;
    }
    .margin_top-10
    {
        margin-top: -10px;
    }
    @media print {
.gray {
    background-color: #dedede !important;
    -webkit-print-color-adjust: exact; 
}}
.pagebreak { page-break-before: always; }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <img src="<?php echo base_url('assets/img/logo.png')?>" class="img-responsive center-block" 
                style="height:80px;position:absolute;left:150px;top:-5px;">
                <center>
                
                <p class="st_joseph_text" style="display:inline-block;">St. Joseph School </p>
               <p style="font-family:Arial;font-size:10px;margin-top:-10px;"><b>141 C. Colago Avenue, San Pablo City 4000</b></p>
               <p style="font-family:Arial;font-size:10px;margin-top:-15px;">Telefax: (049)5628-096</p>
               <p style="font-family:Arial;font-size:10px;margin-top:-15px;">Gmail: st.josephschool@gmail.com</p>
                </center>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <p><b><?php echo strtoupper($full_name);?></b></p>
                <p class="margin_top-10"><?php echo ucwords($designation);?></p>
                <p class="margin_top-10"><?php echo ucwords($school);?></p>
                <p class="margin_top-10"><?php echo ucwords($address);?></p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <!-- <p style="font-size:15px;">Dear <?php echo ucwords($title." ".$full_name)?>:</p>
                <br>
                <p style="font-size:15px;">Congratulations!</p>
                <br>
                <p style="font-size:15px;">We are pleased to inform you that a number of your students passed our <b>Entrance/ Scholarship Examination</b> for this coming Academic Year <?php echo $academic_year_from."-".$academic_year_to;?>. We would like to ask for your support by helping us in disseminating the result of your Grade Six students who took the admission test in our school. Listed below are the names and their Endtrance / Scholarship Results.</p> -->
                <p style="font-size:15px;"><?php echo $content?></p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 ">
                <table class="tg" style="undefined;table-layout: fixed; width: 100%">
                    <colgroup>
                        <col style="width: 20%">
                        <col style="width: 55%">
                        <col style="width: 25%">
                    </colgroup>
                  <tr>
                    <th class="tg-baqh">Name of Student</th>
                    <th class="tg-baqh">Entrance Test</th>
                    <th class="tg-baqh">Scholarship Test</th>
                  </tr>
                   <?php 
                   $count_examinees = count($exams);
                  foreach ($exams as $key) {
                    $middle_initial = substr($key->middle_name,0,1).'.';
                    $full_name2 = strtoupper($key->last_name).', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
                    $scholarship = "Not Qualified";
                    foreach($scholarships as $key2)
                    {
                        if($key->rating >= $key2->min_rating  && $key->rating <= $key2->max_rating)
                        {
                            $scholarship = $key2->scholar_type;
                            break;
                        }
                    }
                    ?>
                  <tr>
                    <td class="tg-yw4l"><?php echo $full_name2?></td>
                    <td class="tg-yw4l"><?php if($key->rating >=75){echo "PASSED";}else{echo "FAILED";}?></td>
                    <td class="tg-yw4l"><?php echo $scholarship?></td>
                    
                  </tr>
                  <?php
                  } ?> 
                </table>
            </div>   
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-12">
                <p style="font-size:15px;">If you have any queries regarding this matter, we will be glad to respond to them so that we can provide your students with necessary information. You may contact us at 049-5030725 or 049-5628096 or yo may personally visit us at 141 C. Colago Avenue, San Pablo City, Laguna. </p>
                <br>
                <p style="font-size:15px;">Thank you for your continuos support and cooperation for our school</p>
            </div>
        </div>
        <div class="pagebreak"> </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-4">
                <p>Sincerely,</p>
                <br>
                <div style="width:30%;border-bottom:2px solid black;"></div>
                <p><?php echo $sincerely_name?></p>
                <p class="margin_top-10"><?php echo $sincerely_designation?></p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <p>Noted by:</p>
                <br>
                <div style="width:50%;border-bottom:2px solid black;"></div>
                <p><?php echo $noted_by_name?></p>
                <p class="margin_top-10"><?php echo $noted_by_designation?></p>
            </div>
            <?php if($noted_by_name2!="" && $noted_by_designation2 != ""){ ?>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <p>Noted by:</p>
                <br>
                <div style="width:50%;border-bottom:2px solid black;"></div>
                <p><?php echo $noted_by_name2?></p>
                <p class="margin_top-10"><?php echo $noted_by_designation2?></p>
            </div>
            <?php } ?>
        </div>
        
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>


</html>
