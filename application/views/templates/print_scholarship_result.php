<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url()?>assets/img/logo.png" />
    <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/logo.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>St. Joseph School SPC</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url()?>material/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>material/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/styles.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href='<?php echo base_url()?>assets/iconfont/material-icons.css' rel='stylesheet' type='text/css'>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url()?>material/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>material/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/global.js"></script>
    <style type="text/css">
    body
    {
        background-color: white;
    }
    table
    {
        width: 100%;
    }
    .st_joseph_text{
        font-family: Old-English;
        font-size: 18px;
        text-align: center;
    }
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    .tg .tg-yw4l{vertical-align:top}
    @media print {
      @page { margin: 0; }
      body { margin: 1.6cm; }
    }
    @page { margin: 0; }
    .sub_text{
        text-align: center;
        font-size: 16px;
    }
    .title_text{
        text-align: center;
        font-size: 16px;
    }
    .date{
        font-size: 16px;
        margin-left: 20px;
    }
    th
    {
        font-weight: bold;
    }
    td
    {
        text-align: center;
    }
    .gray
    {
        background-color:#dedede;
    }
    @media print {
.gray {
    background-color: #dedede !important;
    -webkit-print-color-adjust: exact; 
}}

    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 ">
            <p class="st_joseph_text">St. Joseph School</p>
            <p class="sub_text">San Pablo City</p>
            <br>
            <p class="title_text">ENTRANCE EXAM / SCHOLARSHIP TEST RESULT</p>
            <p class="title_text">For A.Y. <?php echo $academic_year?></p>
            <br>
            <p class="date"><?php echo date("F d, Y");?></p>
                <table class="tg" style="table-layout: fixed; width: 100%">
                    <colgroup>
                        <col style="width: 31%">
                        <col style="width: 31%">
                        <col style="width: 12%">
                        <col style="width: 14%">
                        <col style="width: 12%">
                    </colgroup>
                  <tr>
                    <th class="tg-baqh">Name of School</th>
                    <th class="tg-baqh">Name of Student</th>
                    <th class="tg-baqh">Entrance <br>Test</th>
                    <th class="tg-baqh">Scholarship Test</th>
                    <th class="tg-baqh">Rating</th>
                  </tr>
                  <?php 
                  $last_school =""; //none 
                  $last_school2 =""; //none 
                  $current_school = ""; //yes
                  $x = 0;
                  foreach ($exams as $key) {
                    if($x==0)
                    {
                        $last_school2 = strtolower($key->school); //fit1 
                    }
                    $x++;
                    $current_school = strtolower($key->school); 
                    $middle_initial = substr($key->middle_name,0,1).'.';
                    $full_name = strtoupper($key->last_name).', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
                    $scholarship = "Not Qualified";
                    foreach($scholarships as $key2)
                    {
                        if($key->rating >= $key2->min_rating  && $key->rating <= $key2->max_rating)
                        {
                            $scholarship = $key2->scholar_type;
                            break;
                        }
                    }
                    ?>
                    <?php 
                    if($last_school != strtolower($key->school)) //1. equal
                    {
                  ?>
                  <tr>
                    <td class="tg-yw4l gray"></td>
                    <td class="tg-yw4l gray"></td>
                    <td class="tg-yw4l gray"></td>
                    <td class="tg-yw4l gray"></td>
                    <td class="tg-yw4l gray"></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td class="tg-yw4l"><?php if($last_school!=$current_school){echo ucwords($key->school);}//true?></td> 
                    <td class="tg-yw4l"><?php echo $full_name?></td>
                    <td class="tg-yw4l"><?php if($key->rating >=75){echo "PASSED";}else{echo "FAILED";}?></td>
                    <td class="tg-yw4l"><?php echo $scholarship?></td>
                    <td class="tg-yw4l"><?php echo $key->rating?></td>
                  </tr>

                  
                  <?php
                    $last_school = $current_school;
                  } ?>
                </table>
            </div>   
        </div>
        
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>


</html>
