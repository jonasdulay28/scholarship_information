
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    <h4>Create Letter</h4>
                    </div>
                    <div class="card-content">
                        <?php echo form_open('Create_Letter/print_letter','method="POST" target="_blank"  autocomplete="off"') ?>
                        <h4 class="card-title">Send To</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Title (ex. Mr./Mrs./Sir/Ma'am)</label>
                                        <input type="text" class="form-control" name="title" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name</label>
                                        <input type="text" class="form-control" name="full_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Designation</label>
                                        <input type="text" class="form-control" name="designation" required>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">School</label>
                                        <input type="text" class="form-control" name="school" required>
                                    </div>  
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Address</label>
                                        <input type="text" class="form-control" name="address" required>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="content" id="editor"></textarea>
                                </div>
                            </div>

                        <h4 class="card-title">Examination Details</h4>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">School</label>
                                    <select class="form-control" name="exam_school" required>
                                        <option value='' selected=""></option>
                                        <?php foreach ($schools as $key) {
                                        ?>
                                        <option value='<?php echo ucwords($key->school)?>'><?php echo ucwords($key->school)?></option>
                                        <?php 
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Academic Year From</label>
                                    <input type="number" class="form-control" name="academic_year_from" 
                                    min="2000"  required>
                                </div>  
                            </div>
                            <div class="col-md-1">
                                <div class="form-group" style="text-align:center;">
                                    <label class="control-label" style="font-size:14px;">TO</label>
                                </div>  
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Academic Year To</label>
                                    <input type="number" class="form-control" name="academic_year_to"  min="2000" required>
                                </div>  
                            </div>
                        </div>
                            
                        <h4 class="card-title">From Details</h4>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Sincerely from</label>
                                        <input type="text" class="form-control" name="sincerely_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Designation</label>
                                        <input type="text" class="form-control" name="sincerely_designation" required>
                                    </div>  
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Noted by</label>
                                        <input type="text" class="form-control" name="noted_by_name" required>
                                        
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Designation</label>
                                        <input type="text" class="form-control" name="noted_by_designation" required>
                                        
                                    </div>  
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Noted by (optional)</label>
                                        <input type="text" class="form-control" name="noted_by_name2" >
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Designation (optional)</label>
                                        <input type="text" class="form-control" name="noted_by_designation2">
                                        
                                    </div>  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-fill btn-success " name="submit">Submit</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .catch( error => {
        console.error( error );
    } );


</script>