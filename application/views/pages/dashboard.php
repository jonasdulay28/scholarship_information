<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Scholars</p>
                        <h3 class="title"><?php echo $num_scholars?>
                        </h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons text-info">info</i>
                            <a href="<?php echo base_url('Monitoring')?>">View Information</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">book</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Curriculum</p>
                        <h3 class="title"><?php echo $num_curriculum?></h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons text-info">info</i>
                            <a href="<?php echo base_url('Curriculum/records')?>">View Information</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="card-content">
                        <p class="category">Users</p>
                        <h3 class="title"><?php echo $num_users?></h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons text-info">info</i>
                            <?php if(decrypt($this->session->role)==1){ ?>
                            <a href="<?php echo base_url('Users/records')?>">View Information</a>
                            <?php }else{ ?>
                            <a href="#">Not Available</a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">description</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Examinees</p>
                        <h3 class="title"><?php echo $num_exams?></h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons text-info">info</i>
                            <a href="<?php echo base_url('Examinations/records')?>">View Information</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                         <i class="material-icons" style="display:inline-block;">pie_chart</i>
                         <p class="category" style="display:inline-block;margin-left:10px;margin-top:-20px;font-size:18px;color: rgba(255, 255, 255, 1);">Scholarship Division</p>
                    </div>
                    <div class="card-content">
                    
                    <div class="chart" id="sales-chart" style="min-height: 300px; position: relative;"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="card">
                    <?php 
                    $academic_year_to = date("Y");
        $academic_year_from = $academic_year_to-1;
        $academic_year = $academic_year_from."-".$academic_year_to;?>
                    <div class="card-header" data-background-color="blue">
                         <i class="fa fa-graduation-cap fa-2x" style="display:inline-block;"></i>
                         <p class="category" style="display:inline-block;margin-left:10px;margin-top:-20px;font-size:18px;color: rgba(255, 255, 255, 1);">Top 10 Scholars A.Y. <?php echo $academic_year?></p>
                    </div>
                    <div class="card-content">
                    <ol>
                        <?php 
                        if(!empty($top_10_scholars)){
                        foreach ($top_10_scholars as $key ) {
                        $middle_initial = substr($key->middle_name,0,1).'.';
                         $full_name = $key->last_name.', '.$key->first_name.' '.$middle_initial.' '.$key->suffix;
                        ?>
                        <li style="margin-top: 1rem;"> <?php echo $full_name?> &nbsp; - &nbsp;Final Avg:<b> <?php echo $key->latest_avg?></b></li>
                        <?php 
                        }
                        }
                        else
                        {
                        ?>
                        <li>No final submission made</li>
                        <?php
                        }
                        ?>
                    </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#00a65a", "#fb8c00", "#E9423E"],
      data: <?php echo $scholar_count; ?>,
      hideHover: 'auto'
    });
});
</script>
