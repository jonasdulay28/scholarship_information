<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    <h4>Scholar Information</h4>
                    </div>
                    <div class="card-content">
                        <?php echo form_open('Monitoring/add','method="POST" id="modify_grade" autocomplete="off"') ?>
                            <input type="hidden" value="<?php echo $this->uri->segment(3);?>"  name="scholar_id" class="scholar_id">
                            <input type="hidden" value="<?php echo $this->uri->segment(4);?>"  name="curriculum_id" class="curriculum_id">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" value="<?php echo $scholar->last_name?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label class="control-label">First Name</label>
                                        <input type="text" class="form-control" name="first_name" value="<?php echo $scholar->first_name?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="control-label">Middle Name</label>
                                        <input type="text" class="form-control" name="middle_name" value="<?php echo $scholar->middle_name?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <label class="control-label">Suffix</label>
                                        <input type="text" class="form-control" name="suffix" value="<?php echo $scholar->suffix?>" readonly>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="control-label">Student Number</label>
                                        <input type="text" class="form-control" name="school" value="<?php echo $scholar->student_number?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Section</label>
                                        <input type="text" class="form-control" name="section" value="<?php if(empty($scholar_grades)){echo $scholar->section;}else{echo $scholar_grades->section;}?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Academic Year</label>
                                        <input type="text" class="form-control" name="academic_year" value="<?php 
                                        if(empty($scholar_grades)){echo $scholar->scholar_academic_year_from.'-'.$scholar->scholar_academic_year_to;}else{echo $scholar_grades->academic_year;}?>" readonly>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Scholarhip Type</label>
                                        <input type="text" class="form-control" name="scholarship_type" value="
                                        <?php if(empty($scholar_grades)){echo $scholar->scholar_type;}else{echo $scholar_grades->scholar_type;}?>" readonly>
                                    </div>  
                                </div>
                            </div>
                            <?php if(!empty($scholar_curriculum))
                            { ?>
                            <div class="row">
                                <div class="col-md-12">
                                 <h4 class="card-title">Curriculum:</h4>
                                    <ul>
                                        <?php 
                                            foreach($scholar_curriculum as $key)
                                            { 
                                                $scholar_id = decrypt($this->uri->segment(3));
                                                $curriculum_id = decrypt($this->uri->segment(4));

                                                $enc_scholar_id = encrypt($key->scholar_id);
                                                $enc_curriculum_id = encrypt($key->curriculum_id);
                                                if(($scholar_id==$key->scholar_id) && ($curriculum_id==$key->curriculum_id))
                                                {
                                            ?>
                                                <li><a href="javascript:void(0);" style="color:black;cursor:default;">A.Y. <?php echo $key->academic_year;?></a></li>
                                        <?php 
                                                }
                                                else
                                                { ?>
                                                <li><a href="<?php echo base_url('Monitoring/view/').$enc_scholar_id.'/'.$enc_curriculum_id;?>">A.Y. <?php echo $key->academic_year;?></a></li>
                                        <?php   } 
                                            }
                                         ?>
                                    </ul>
                                </div>
                            </div>
                            <?php } ?>
                            <h4 class="card-title">Current Curriculum:</h4>
                            <?php 
                                if(empty($scholar_grades))
                                {
                                    $subjects = explode(";",$scholar->subjects); 
                                    array_push($subjects, "Final Avg.");
                                    $new_subjects = implode(";",$subjects);
                                }
                                else
                                {
                                    $subjects = explode(";",$scholar_grades->subjects); 
                                    $new_subjects = implode(";",$subjects);
                                }
                                
                                
                            ?>
                            <input type="hidden" name="subjects" value="<?php echo $new_subjects?>">

                            <?php
                            $grade="";
                                if(empty($scholar_grades))
                                {
                                    $grade=0;
                                }
                                else
                                {
                                    $grade = explode(";", $scholar_grades->grades);

                                }
                            $subject_avg = array();
                            $y=0;
                            $first_grading = array();
                            $second_grading = array();
                            $third_grading = array();
                            $fourth_grading = array();
                            $final_average = array();
                            $real_subjects = count($subjects) - 1;
                            for($x=0;$x<count($subjects);$x++){ 
                                
                                 $i = $x+1;
                                $grading_avg_array = array();
                                if($i == count($subjects)){
                             ?>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label" style="color:black;font-size:13px;"><?php echo $subjects[$x]?></label>
                                        </div>  
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">1st Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{
                                                echo number_format((float)array_sum($first_grading)/$real_subjects, 2, '.', '');
                                                $y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?> style="font-weight:bold;<?php if(number_format((float)array_sum($first_grading)/$real_subjects, 2, '.', '') >= 85){echo 'color:green';}else{echo 'color:red';}?>" readonly>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">2nd Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo number_format((float)array_sum($second_grading)/$real_subjects, 2, '.', '');$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?> style="font-weight:bold;<?php if(number_format((float)array_sum($second_grading)/$real_subjects, 2, '.', '') >= 85){echo 'color:green';}else{echo 'color:red';}?>" readonly>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">3rd Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo number_format((float)array_sum($third_grading)/$real_subjects, 2, '.', '');$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?> style="font-weight:bold;<?php if(number_format((float)array_sum($third_grading)/$real_subjects, 2, '.', '') >= 85){echo 'color:green';}else{echo 'color:red';}?>" readonly>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">4th Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo number_format((float)array_sum($fourth_grading)/$real_subjects, 2, '.', '');$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?> style="font-weight:bold;<?php if(number_format((float)array_sum($fourth_grading)/$real_subjects, 2, '.', '') >= 85){echo 'color:green';}else{echo 'color:red';}?>" readonly>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">Average </label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo number_format((float)array_sum($final_average)/$real_subjects, 2, '.', '');$y++;}?>" min="0" max="100" step="any" required readonly style="font-weight:bold;<?php if(number_format((float)array_sum($final_average)/$real_subjects, 2, '.', '') >= 85){echo 'color:green';}else{echo 'color:red';}?>" readonly>
                                        </div> 
                                    </div>
                                </div>
                            <?php
                                }
                                else
                                { ?>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label" style="color:black;font-size:13px;"><?php echo $subjects[$x]?></label>
                                        </div>  
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">1st Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo $grade[$y];array_push($grading_avg_array, $grade[$y]);array_push($first_grading, $grade[$y]);$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?>>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">2nd Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo $grade[$y];array_push($grading_avg_array, $grade[$y]);array_push($second_grading, $grade[$y]);$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?>>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">3rd Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo $grade[$y];array_push($grading_avg_array, $grade[$y]);array_push($third_grading, $grade[$y]);$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?>>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">4th Grading</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo $grade[$y];array_push($grading_avg_array, $grade[$y]);array_push($fourth_grading, $grade[$y]);$y++;}?>" step="any" min="0" max="100" required <?php if(!empty($scholar_grades) && $x+1 == count($subjects)){echo "readonly";}?>>
                                        </div> 
                                    </div>
                                    <div class="col-md-2">
                                         <div class="form-group label-floating">
                                            <label class="control-label">Average</label>
                                            <input type="number" class="form-control" name="grades[]" value="<?php if(empty($scholar_grades)){echo $grade;}else{echo array_sum($grading_avg_array)/4;array_push($final_average, array_sum($grading_avg_array)/4);$y++;}?>" min="0" max="100" step="any" required readonly style="font-weight:bold;<?php if(array_sum($grading_avg_array)/4 >= 85){echo 'color:green';}else{echo 'color:red';}?>">
                                        </div> 
                                    </div>
                                </div>
                            <?php
                                }
                            }
                            ?>

                            
                            <br>
                            <?php if(empty($scholar_grades) ){ ?>
                            <button type="submit" class="btn btn-fill btn-success modify_grade_btn " name="submit" >Update</button>
                            <button type="button" class="btn btn-fill btn-warning final_submit_btn " name="submit" >Final Submission</button>
                            <?php 
                            }elseif($scholar_grades->status == 1)
                            { ?> 
                                <button type="submit" class="btn btn-fill btn-success modify_grade_btn " name="submit" >Update</button>
                                <button type="button" class="btn btn-fill btn-warning final_submit_btn " name="submit" >Final Submission</button>
                            <?php 
                            }
                            else
                            {
                                if(decrypt($this->session->role)==1)
                                {
                            ?>
                                <button type="button" class="btn btn-fill btn-warning update_final_submit_btn " name="submit" data-id = "<?php echo encrypt($scholar_grades->id);?>" >Remodify</button>
                            <?php 
                                }
                                else{
                                echo "<p>This was already submitted. Please contact the administrator if you want to update the grade.</p>";
                                }
                            }
                            ?>
                            <button type="button" class="btn btn-fill btn-info print_grade " name="">Print</button>
                        <?php echo form_close(); ?>
                        <?php echo form_open('Monitoring/print_grade','method="POST" target="_blank"  autocomplete="off" style="display: inline-block;"') ?>
                            <input type="hidden" value="<?php echo $this->uri->segment(3);?>"  name="scholar_id" class="scholar_id">
                            <input type="hidden" value="<?php echo $this->uri->segment(4);?>"  name="curriculum_id" class="curriculum_id">
                            <button type="submit" class="btn btn-fill btn-info print_grade_btn" name="submit" style="display: none;">Print</button>
                        <?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".update_final_submit_btn").on("click",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Monitoring/update_final_submit")?>';
        var id = $(this).data('id');
        swal({
          title: 'Are you sure?',
          text: "The grades will be available for modifications",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirm'
        }).then((result) => {
            $.ajax({
                type : 'POST',
                url : post_url,
                data: {id:id},
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        location.reload();
                    else
                        notify2("Failed","Remodify failed","error");

                },
                error : function(res) {
                    console.log(res);
                    notify2("Failed","Remodify failed","error");
                }
            });
        });
    });
    $(".final_submit_btn").on("click",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Monitoring/final_submit")?>';

        swal({
          title: 'Are you sure?',
          text: "Grade of the student cannot be modified after the submission",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirm'
        }).then((result) => {
            $.ajax({
                type : 'POST',
                url : post_url,
                data: $('#modify_grade').serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        location.reload();
                    else
                        notify2("Failed","Final Submission failed","error");

                },
                error : function() {
                    notify2("Failed","Final Submission failed","error");
                }
            });
        });
    });
    $(".print_grade").on("click",function(e)
    {
        e.preventDefault();
        $(".print_grade_btn").click();
    });
/*    $("#modify_grade_btn").one("submit",function(e)
    {
        e.preventDefault();
        swal({
          title: 'Are you sure?',
          text: "This will update the grade of the student",
          type: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirm'
        }).then((result) => {
            if ($("#modify_grade").valid()) { 
                e.preventDefault(); 
               //form was NOT ok - optionally add some error script in here
               return false; //for old browsers 
            } else{
               $("#modify_grade").submit();e
            }
           
        });
        
    });*/
</script>