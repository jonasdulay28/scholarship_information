<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    <h4>Add User</h4>
                    </div>
                    <div class="card-content">
                        <?php echo form_open('Users/add','method="POST" id="add_user" autocomplete="off"') ?>
                        <h4 class="card-title">User Information</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">First Name</label>
                                        <input type="text" class="form-control" name="first_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Middle Name</label>
                                        <input type="text" class="form-control" name="middle_name">
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Suffix</label>
                                        <input type="text" class="form-control" name="suffix">
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Full Address</label>
                                        <input type="text" class="form-control" name="address" required>
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Contact Number</label>
                                        <input type="number" class="form-control" name="contact_number" required>
                                    </div>  
                                </div>
                            </div>
                        <h4 class="card-title">Account Information</h4>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email Address</label>
                                        <input type="email" class="form-control" name="email_address" required>
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">User Role</label>
                                        <select class="form-control" name="role" required>
                                            <option value='' selected=""></option>
                                            <option value="1">Administrator</option>
                                            <option value="2">Encoder</option>
                                         </select>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Password</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                        minlength="6" maxlength="20" required>
                                    </div>  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirm Password</label>
                                        <input type="password" class="form-control" name="confirm_password" id="confirm_password" minlength="6" maxlength="20" required>
                                    </div>  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-fill btn-success " name="submit">Submit</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
    set_csrf('<?php echo $this->security->get_csrf_hash()?>');
    $("#add_user").on("submit",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Users/add")?>';
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#add_user').serialize(),
            dataType:"json",
            beforeSend:function(){
                loading();
            },
            success : function(res){
                close_loading();
                if(res.message=="success")
                    notify2("Success","User added successfully","success");
                else
                    notify2("Failed","User added failed","error");

                $('#add_user').each(function() { this.reset() });
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });
</script>