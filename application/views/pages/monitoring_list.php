<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<div class="card">
                    <div class="card-header" data-background-color="green">
	                    <h4 class="title">List of Scholars</h4>
	                </div>
                    <div class="card-content">
                        <?php echo form_open("Monitoring/print_active_scholars",'target="_blank" method="POST"'); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:10px;">
                                    <button type="button" class="btn btn-fill btn-rose "data-toggle="modal" data-target="#addScholar">
                                    <i class="fa fa-graduation-cap"></i> Add Scholar</button>
                                </div>   
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:0px;">
                                    <label class="control-label">Sort</label>
                                    <select class="form-control sort_curriculum" name="curriculum">
                                        <option value="">All</option>
                                        <?php foreach($curriculum as $key)
                                        { ?>
                                            <option value="<?php echo $key->id;?>"><?php echo $key->curriculum?></option>
                                        <?php } ?>
                                    </select>
                                </div>  
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top:0px;">
                                    <label class="control-label">Academic Year From</label>
                                    <input type="number" class="form-control" name="academic_year_from" required>
                                </div>  
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top:0px;">
                                    <label class="control-label">Academic Year To</label>
                                    <input type="text" class="form-control" name="academic_year_to" required>
                                </div>  
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top:10px;">
                                    <button type="submit" class="btn btn-fill btn-success " name="submit"><i class="fa fa-print"></i></button>
                                </div>  
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="material-datatable table-responsive">
                            <table id="scholars" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Student#</th>
                                        <th>Name</th>
                                        <th>Current Curriculum</th>
                                        <th>Section</th>
                                        <th>Average</th>
                                        <th>Scholarship</th>
                                        <th>A.Y</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Student#</th>
                                        <th>Name</th>
                                        <th>Current Curriculum</th>
                                        <th>Section</th>
                                        <th>Average</th>
                                        <th>Scholarship</th>
                                        <th>A.Y</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="addScholar">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Scholar</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="add_scholar" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <h4 class="card-title">Scholar Information</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="add_last_name" required>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="add_first_name" required>
                </div>  
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" id="add_middle_name" >
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Suffix</label>
                    <input type="text" class="form-control" name="suffix" id="add_suffix" >
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Student Number</label>
                    <input type="text" class="form-control" name="student_number" id="add_student_number" required>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Scholarship</label>
                    <select class="form-control" name="scholarship_type" id="add_scholarship_type" required>
                        <option value="">None</option>
                        <?php 
                        foreach($scholarship as $key)
                        {
                        ?>
                            <option value="<?php echo $key->id;?>"><?php echo $key->scholar_type;?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Date of Scholarship</label>
                    <input type="date" class="form-control" name="created_at" id="add_created_at" required>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group ">
                    <label class="control-label">Academic Year From</label>
                    <input type="number" class="form-control" name="academic_year_from" 
                    min="2000" id="add_academic_year_from"  required>
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group" style="text-align:center;">
                    <label class="control-label" style="font-size:14px;">TO</label>
                </div>  
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Academic Year To</label>
                    <input type="number" class="form-control" name="academic_year_to"  min="2000"
                    id="add_academic_year_to" required>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Current Curriculum</label>
                    <select class="form-control" name="curriculum_id" id="add_curriculum" required>
                        <option value=""></option>
                        <?php foreach($curriculum as $key)
                        { ?>
                            <option value="<?php echo $key->id;?>"><?php echo $key->curriculum?></option>
                        <?php } ?>
                    </select>
                </div>  
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Section</label>
                    <input type="text" class="form-control" name="section" id="add_section" required>
                </div>  
            </div>
        </div>
        <h4 class="card-title none">Exam Information</h4>
        <div class="row none">
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Score/Rating</label>
                    <input type="number" class="form-control" name="rating" id="add_rating" value="0" readonly="">
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Date of Examination</label>
                    <input type="date" class="form-control date" name="date" id="add_date_exam" value="<?php echo date('Y-m-d'); ?>" readonly="">
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Contact Number</label>
                    <input type="number" class="form-control date" name="contact_number" id="add_contact_number" >
                </div>  
            </div>
        </div>
    </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="scholarModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modify Scholar</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_scholar" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="edit_id" name="id" readonly="" />
        <h4 class="card-title">Scholar Information</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="edit_last_name" readonly>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="edit_first_name" readonly>
                </div>  
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" id="edit_middle_name" readonly>
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Suffix</label>
                    <input type="text" class="form-control" name="suffix" id="edit_suffix" readonly>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Student Number</label>
                    <input type="text" class="form-control" name="student_number" id="edit_student_number">
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Scholarship</label>
                    <select class="form-control" name="scholarship_type" id="edit_scholarship_type">
                        <option value="">None</option>
                        <?php 
                        foreach($scholarship as $key)
                        {
                        ?>
                            <option value="<?php echo $key->id;?>"><?php echo $key->scholar_type;?></option>
                        <?php 
                        } 
                        ?>
                    </select>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Date of Scholarship</label>
                    <input type="text" class="form-control" name="created_at" id="edit_created_at">
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group ">
                    <label class="control-label">Academic Year From</label>
                    <input type="number" class="form-control" name="academic_year_from" 
                    min="2000" id="edit_academic_year_from"  required>
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group" style="text-align:center;">
                    <label class="control-label" style="font-size:14px;">TO</label>
                </div>  
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">Academic Year To</label>
                    <input type="number" class="form-control" name="academic_year_to"  min="2000"
                    id="edit_academic_year_to" required>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Current Curriculum</label>
                    <select class="form-control" name="curriculum_id" id="edit_curriculum">
                        <option value=""></option>
                        <?php foreach($curriculum as $key)
                        { ?>
                            <option value="<?php echo $key->id;?>"><?php echo $key->curriculum?></option>
                        <?php } ?>
                    </select>
                </div>  
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Section</label>
                    <input type="text" class="form-control" name="section" id="edit_section" >
                </div>  
            </div>
        </div>
        <h4 class="card-title">Exam Information</h4>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Score/Rating</label>
                    <input type="number" class="form-control" name="rating" id="edit_rating" readonly="">
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Date of Examination</label>
                    <input type="date" class="form-control date" name="date" id="edit_date_exam" readonly="">
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">Contact Number</label>
                    <input type="number" class="form-control date" name="contact_number" id="edit_contact_number" >
                </div>  
            </div>
        </div>
    </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
set_csrf('<?php echo $this->security->get_csrf_hash()?>');
	function getScholars()
    {
       $('#scholars').DataTable({
      		"ajax": "<?php echo base_url('Monitoring/getScholars')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
	$(function($) {
		getScholars();
		var table = $('#scholars').DataTable();
		table.on('click', '.activate, .deactivate', function(e)
		{
			var id = $(this).data('id');
	        var post_url = '<?php echo base_url()?>Monitoring/update_status';
	        swal({
			  title: 'Are you sure?',
			  text: "Scholar status will be updated.",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirm'
			}).then((result) => {
			  	$.ajax({
		            type : 'POST',
		            url : post_url,
		            data:{id: id},
		            dataType:"json",
		            beforeSend:function(){
		            	loading();
		            },
		            success : function(res){
		            	close_loading();
		                if(res.message=="success")
		                {
		                    $("#scholars").dataTable().fnDestroy();
		                    getScholars();
		                    notify2("Success","Scholar status has been updated successfuly","success");
		                }
		                else
		                {   
		                    notify2("Failed","Scholar status updated failed","error");
		                }

		            },
		            error : function() {
		                notify2("Failed","Scholar status updated failed","error");
		            }
		        });
			});
	        
		});


        table.on('click', '.modify', function(e)
        {
            e.preventDefault();
            var id = $(this).data('id');
            var post_url = '<?php echo base_url()?>Monitoring/get_scholar';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{id: id},
                dataType:"json",
                beforeSend:function(){
                },
                success : function(res){
                    var scholar_date_arr = res.scholar.created_at.split(" ");
                    $("#edit_id").val(id);
                    $("#edit_first_name").val(res.scholar.first_name);
                    $("#edit_last_name").val(res.scholar.last_name);
                    $("#edit_middle_name").val(res.scholar.middle_name);
                    $("#edit_suffix").val(res.scholar.suffix);
                    $("#edit_student_number").val(res.scholar.student_number);
                    $("#edit_scholarship_type").val(res.scholar.scholarship_id);
                    $("#edit_created_at").val(scholar_date_arr[0]);
                    $("#edit_curriculum").val(res.scholar.curriculum_id);
                    $("#edit_section").val(res.scholar.section);
                    $("#edit_rating").val(res.scholar.rating);
                    $("#edit_date_exam").val(res.scholar.exam_date);
                    $("#edit_academic_year_from").val(res.scholar.scholar_academic_year_from);
                    $("#edit_academic_year_to").val(res.scholar.scholar_academic_year_to);
                    if(res.scholar.scholar_contact_number=="")
                        $("#edit_contact_number").val(res.scholar.contact_number);
                    else
                        $("#edit_contact_number").val(res.scholar.scholar_contact_number);
                },
                error : function() {
                }
            });
        });
        $("#modify_scholar").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url()?>Monitoring/update_scholar';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:$("#modify_scholar").serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                    {
                        $("#scholars").dataTable().fnDestroy();
                        getScholars();
                        notify2("Success","Scholar updated successfuly","success");
                        $("#scholarModal").modal("toggle");
                    }
                    else
                    {
                        notify2("Failed","Scholar update falied","error");
                    }

                },
                error : function() {
                    notify2("Failed","Scholar status update falied","error");
                }
            });
        });

        $(".sort_curriculum").on("change",function(e)
        {
            e.preventDefault;
            var curriculum = $(".sort_curriculum").val();
            var post_url = '<?php echo base_url()?>Monitoring/sort_scholar';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{curriculum:curriculum},
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    console.log(res);
                   $("#scholars").dataTable().fnDestroy();
                        $('#scholars').DataTable({
                            "data": res.data,
                            "deferRender": true,
                            "stateSave": true,
                            "order":[],/*
                            "columnDefs": [ {
                            "targets": 8,
                            "orderable": false
                            },*/
                            "pagingType": "full_numbers",
                            "lengthMenu": [
                                [10, 25, 50, -1],
                                [10, 25, 50, "All"]
                            ],
                            responsive: true,
                            language: {
                                search: "_INPUT_",
                                searchPlaceholder: "Search records",
                            }

                        });

                },
                error : function() {
                    notify2("Failed","Sort scholars falied","error");
                }
            });
        });

        $("#add_scholar").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url("Monitoring/add_scholar")?>';
            $.ajax({
                type : 'POST',
                url : post_url,
                data: $('#add_scholar').serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        notify2("Success","Scholar added successfully","success");
                    else
                        notify2("Failed","Scholar added failed","error");

                    $("#scholars").dataTable().fnDestroy();
                    getScholars();
                    $('#add_scholar').each(function() { this.reset() });
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        });
  	});
</script>