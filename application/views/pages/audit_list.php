<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<div class="card">
                    <div class="card-header" data-background-color="green">
	                    <h4 class="title">Audit Trails</h4>
	                </div>
                    <div class="card-content">
                        <div class="material-datatable table-responsive">
                            <table id="audit_trails" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>User</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>User</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
	function get_curriculum()
    {
       $('#audit_trails').DataTable({
      		"ajax": "<?php echo base_url('Settings/getAudit')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    $(function($) {
    get_curriculum();
    });
</script>