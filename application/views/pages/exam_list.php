<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<div class="card">
                    <div class="card-header" data-background-color="green">
	                    <h4 class="title">List of Exams</h4>
	                </div>
                    <div class="card-content">
                        <?php echo form_open("Exams/print_exam_results",'target="_blank" method="POST"'); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:0px;">
                                    <label class="control-label">Academic Year From</label>
                                    <input type="number" class="form-control" name="academic_year_from" required>
                                </div>  
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:0px;">
                                    <label class="control-label">Academic Year To</label>
                                    <input type="text" class="form-control" name="academic_year_to" required>
                                </div>  
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="margin-top:10px;">
                                    <button type="number" class="btn btn-fill btn-success " name="submit"><i class="fa fa-print"></i></button>
                                </div>  
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="material-datatable table-responsive">
                            <table id="exams" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Name</th>
                                        <th>School</th>
                                        <th>Contact#</th>
                                        <th>Rating</th>
                                        <th>Scholarship</th>
                                        <th>Date</th>
                                        <th>A.Y</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Name</th>
                                        <th>School</th>
                                        <th>Contact#</th>
                                        <th>Rating</th>
                                        <th>Scholarship</th>
                                        <th>Date</th>
                                        <th>A.Y</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="examModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modify Exam</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_exam" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="edit_id" name="id" readonly="" />
        <h4 class="card-title">Examinee Information</h4>
            <div class="row">
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="edit_last_name" required>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="edit_first_name" required>
                </div>  
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" id="edit_middle_name">
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Suffix</label>
                    <input type="text" class="form-control" name="suffix" id="edit_suffix">
                </div>  
            </div>
        </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label">Name of School</label>
                        <input type="text" class="form-control" name="school" id="edit_school" required>
                    </div>  
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">Contact Number</label>
                        <input type="number" class="form-control" name="contact_number" id="edit_contact_number" required>
                    </div>  
                </div>
            </div>
        <h4 class="card-title">Exam Information</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label">Score/Rating</label>
                        <input type="number" class="form-control" name="rating" id="edit_rating" required>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label class="control-label">Date of Examination</label>
                        <input type="date" class="form-control date" name="date" id="edit_date" required>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group ">
                        <label class="control-label">Academic Year From</label>
                        <input type="number" class="form-control" name="academic_year_from" 
                        min="2000" id="edit_academic_year_from"  required>
                    </div>  
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="text-align:center;">
                        <label class="control-label" style="font-size:14px;">TO</label>
                    </div>  
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label">Academic Year To</label>
                        <input type="number" class="form-control" name="academic_year_to"  min="2000"
                        id="edit_academic_year_to" required>
                    </div>  
                </div>
            </div>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
	function get_exams()
    {
       $('#exams').DataTable({
      		"ajax": "<?php echo base_url('Exams/getExams')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
	$(function($) {
		get_exams();
		var table = $('#exams').DataTable();
		table.on('click', '.activate', function(e)
		{
			var id = $(this).data('id');
            var type = $(this).data('type');
            var yearto = $(this).data('yearto');
            var yearfrom = $(this).data('yearfrom');
	        var post_url = '<?php echo base_url()?>Exams/update_status';
	        swal({
			  title: 'Are you sure?',
			  text: "Examinee will be added to Scholars.",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirm'
			}).then((result) => {
			  	$.ajax({
		            type : 'POST',
		            url : post_url,
		            data:{id: id, type: type,yearfrom:yearfrom,yearto:yearto},
		            dataType:"json",
		            beforeSend:function(){
		            	loading();
		            },
		            success : function(res){
		            	close_loading();
		                if(res.message=="success")
		                {
		                    $("#exams").dataTable().fnDestroy();
		                    get_exams();
		                    notify2("Success","Examinee has been added to scholars successfuly","success");
		                }
		                else
		                {   
		                    notify2("Failed","Examinee status update failed","error");
		                }

		            },
		            error : function() {
		                notify2("Failed","Examinee status update failed","error");
		            }
		        });
			});
	        
		});


        table.on('click', '.modify', function(e)
        {
            e.preventDefault();
            var id = $(this).data('id');
            var post_url = '<?php echo base_url()?>Exams/get_exam';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{id: id},
                dataType:"json",
                beforeSend:function(){
                },
                success : function(res){
                    $("#edit_id").val(id);
                    $("#edit_first_name").val(res.exam.first_name);
                    $("#edit_last_name").val(res.exam.last_name);
                    $("#edit_middle_name").val(res.exam.middle_name);
                    $("#edit_suffix").val(res.exam.suffix);
                    $("#edit_school").val(res.exam.school);
                    $("#edit_contact_number").val(res.exam.contact_number);
                    $("#edit_rating").val(res.exam.rating);
                    $("#edit_date").val(res.exam.exam_date);
                    $("#edit_academic_year_to").val(res.exam.academic_year_to);
                    $("#edit_academic_year_from").val(res.exam.academic_year_from);
                    /*$("#edit_name").val(res.user.name);
                    $("#edit_email").val(res.user.email);
                    $("#edit_address").val(res.user.address);
                    $("#edit_contact").val(res.user.contact_number);
                    $("#edit_role").val(res.user.role_id);
                    $("#edit_branch").val(res.user.branch_id);
                    $("#edit_oauth_uid").val(oauth_uid);
                    $("#created").html("<label>Created</label><br>"+res.user.created);
                    $("#modified").html("<label>Modified</label><br>"+res.user.modified);*/
                },
                error : function() {
                }
            });
        });
        $("#modify_exam").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url()?>Exams/update_exam';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:$("#modify_exam").serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                    {
                        $("#exams").dataTable().fnDestroy();
                        get_exams();
                        notify2("Success","Exam updated successfuly","success");
                        $("#examModal").modal("toggle");
                    }
                    else
                    {
                        notify2("Failed","Exam update falied","error");
                    }

                },
                error : function() {
                    notify2("Failed","Exam status update falied","error");
                }
            });
        })
  	});
</script>