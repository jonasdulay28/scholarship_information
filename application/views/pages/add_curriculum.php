<style type="text/css">

</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    <h4>Add Curriculum</h4>
                    </div>
                    <div class="card-content">
                        <?php echo form_open('Curriculum/add','method="POST" id="add_curriculum" autocomplete="off"') ?>
                        <h4 class="card-title">Curriculum Information</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Curriculum ( ex. Grade 7 (2018) )</label>
                                        <input type="text" class="form-control" name="curriculum" required>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Additional Information</label>
                                        <textarea class="form-control" style="height:150px;" name="description"></textarea>
                                    </div>  
                                </div>
                            </div>
                        <h4 class="card-title">Subjects Included</h4>
                            <div class="subjects">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Subject 1</label>
                                            <input type="text" class="form-control" name="subject[]" required>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Subject 2</label>
                                            <input type="text" class="form-control" name="subject[]" required>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Subject 3</label>
                                            <input type="text" class="form-control" name="subject[]" required>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Subject 4 </label>
                                            <input type="text" class="form-control" name="subject[]" required>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Subject 5</label>
                                            <input type="text" class="form-control" name="subject[]" required>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-fill btn-rose add" 
                                    id="add">Add Subject</button>
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-fill btn-success " name="submit">Submit</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $("#add_curriculum").on("submit",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Curriculum/add")?>';
        swal({
          title: 'Are you sure?',
          text: "This will not be edittable because it can affect the monitoring if theres a changes made. Please recheck it first.",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirm'
        }).then((result) => {
            $.ajax({
                type : 'POST',
                url : post_url,
                data: $('#add_curriculum').serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                        notify2("Success","Curriculum added successfully","success");
                    else
                        notify2("Failed","Curriculum added failed","error");

                    $('#add_curriculum').each(function() { this.reset() });
                },
                error : function() {
                    notify2("Failed","Curriculum added failed","error");
                }
            });
        });
        
    });
    $(document).ready(function(){
        var maxField = 20; //Input fields increment limitation
        var addButton = $('.add'); //Add button selector
        var wrapper = $('.subjects'); //Input field wrapper
        
        var x = 5; //Initial field counter is 1
        $(addButton).click(function(){ //Once add button is clicked
            if(x < maxField){ //Check maximum number of input fields
                x++; //Increment field counter
                var fieldHTML = '<div class="row"> <div class="col-md-10"> <div class="form-group label-floating"> <label class="control-label">Subject '+x+'</label> <input type="text" class="form-control" name="subject[]" required> </div></div><div class="col-md-2"> <button type="submit" class="btn btn-fill btn-danger remove_button" name="submit">Remove</button> </div></div>'; //New input field html 
                $(wrapper).append(fieldHTML); // Add field html
            }
            else
            {
                notify2("Maximum Subjects Allowed","You have reached the maximum fields allowed","error")
            }
        });
        $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });
</script>