<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<div class="card">
                    <div class="card-header" data-background-color="green">
	                    <h4 class="title">List of Curriculum</h4>
	                </div>
                    <div class="card-content">
                        <div class="material-datatable table-responsive">
                            <table id="curriculum" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Curriculum</th>
                                        <th>Additional Info</th>
                                        <th>Subjects</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Curriculum</th>
                                        <th>Additional Info</th>
                                        <th>Subjects</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-left">Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="scholarshipModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modify Scholarship</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_scholarship" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
                <input type="hidden" id="edit_id" name="id" readonly="" />
        <div class="row">
            <div class="col-md-12">
                <div class="form-group ">
                    <label class="control-label">Scholarship</label>
                    <input type="text" class="form-control" id="edit_scholarship" name="scholarship" required>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Minimum Rating</label>
                    <input type="number" class="form-control" id="edit_min_rating" name="min_rating" required>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Maximum Rating</label>
                    <input type="number" class="form-control" id="edit_max_rating" name="max_rating" required>
                </div> 
            </div>
        </div>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
	function get_curriculum()
    {
       $('#curriculum').DataTable({
      		"ajax": "<?php echo base_url('Curriculum/getCurriculum')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
	$(function($) {
		get_curriculum();
		var table = $('#curriculum').DataTable();
		table.on('click', '.activate, .deactivate', function(e)
		{
			var id = $(this).data('id');
	        var post_url = '<?php echo base_url()?>Curriculum/update_status';
	        swal({
			  title: 'Are you sure?',
			  text: "The curriculum status will be updated",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirm'
			}).then((result) => {
			  	$.ajax({
		            type : 'POST',
		            url : post_url,
		            data:{id: id},
		            dataType:"json",
		            beforeSend:function(){
		            	loading();
		            },
		            success : function(res){
		            	close_loading();
		                if(res.message=="success")
		                {
		                    $("#curriculum").dataTable().fnDestroy();
		                    get_curriculum();
		                    notify2("Success","curriculum status updated successfuly","success");
		                }
		                else
		                {
		                    notify2("Failed","curriculum status update failed","error");
		                }

		            },
		            error : function(res) {
                        console.log(res);
		                notify2("Failed","curriculum status update failed","error");
		            }
		        });
			});
	        
		});
  	});
</script>