<!DOCTYPE html>
<html lang="en">
<head>
	<title>St. Joseph School SPC</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/vendor/select2/select2.min.css">
	<!-- Morris chart -->
    <link rel="stylesheet" href="http://stmichaelwater.com/plugins/morris/morris.css">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/css/util.css">
	<link href="<?php echo base_url()?>assets/css/sweetalert2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>login/css/main.css">
	<script src="<?php echo base_url()?>material/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url()?>material/assets/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url()?>assets/js/global.js"></script>
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<!--<div class="wrap-input100 padding0 wrap_login_header" >
					<h1 class="login_heading ">St. Joseph School </h1>
					<p class="login_sub_heading ">Scholarship Information System</p>
				</div> 
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url()?>login/images/img-01.png" alt="IMG">
				</div> -->

				<?php echo form_open('','class="login100-form validate-form" id="login_form" autocomplete="off"');?>
					<span class="login100-form-title">
						<img src="<?php echo base_url()?>assets/img/logo2.jpg" style="height:120px;width: 100px;">
					</span>
					<span class="login100-form-title">
						User Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Email address is required">
						<input class="input100" type="email" name="email" placeholder="Email Address">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100" >
						<center><p style="color:red;" class="error"></p></center>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
					</div>

				<?php echo form_close();?>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/js/sweetalert2.min.js"></script>
	<script src="<?php echo base_url()?>login/vendor/bootstrap/js/popper.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>login/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>login/js/main.js"></script>
	<script>
        $(document).ready(function()
        {
          $('#login_form').submit(function(e){
            e.preventDefault();
            var post_url = '<?php echo base_url()?>Home/index';
              $.ajax({
                  type : 'POST',
                  url : post_url,
                  data: $('#login_form').serialize(),
                  dataType:"json",
                  beforeSend:function(){
                  	loading();
                  },
                  success : function(res){
                  	close_loading();
                    if(res.message=="success")
                    {
                      window.location.href= res.url; // the redirect goes here
                    }
                    else
                    {
                    	$(".error").html(res.message);
                    }
                      
                  },
                  error : function(res) {
                       console.log(res);
                  }
              });
          });
        });
    </script> 
</body>
</html>