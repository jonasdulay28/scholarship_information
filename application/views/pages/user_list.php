<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<div class="card">
                    <div class="card-header" data-background-color="green">
	                    <h4 class="title">List of Users</h4>
	                </div>
                    <div class="card-content">
                        <div class="material-datatable table-responsive">
                            <table id="users" class="table table-striped table-no-bordered table-hover table-responsive" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact#</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact#</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th class="disabled-sorting text-center">Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
        </div>

    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="userModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modify Scholarship</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <?php echo form_open('','id="modify_user" autocomplete="off" method="POST"');?>
      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="edit_id" name="id" readonly="" />
        <h4 class="modal-title">User Information</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name" id="edit_last_name" required>
                </div>  
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="edit_first_name" required>
                </div>  
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="control-label">Middle Name</label>
                    <input type="text" class="form-control" name="middle_name" id="edit_middle_name">
                </div>  
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label class="control-label">Suffix</label>
                    <input type="text" class="form-control" name="suffix" id="edit_suffix">
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group ">
                    <label class="control-label">Full Address</label>
                    <input type="text" class="form-control" name="address" id="edit_address" required>
                </div>  
            </div>
            <div class="col-md-5">
                <div class="form-group ">
                    <label class="control-label">Contact Number</label>
                    <input type="number" class="form-control" name="contact_number" id="edit_contact_number" required>
                </div>  
            </div>
        </div>
        <h4 class="card-title">Account Information</h4>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group ">
                    <label class="control-label">Email Address</label>
                    <input type="email" class="form-control" name="email_address" id="edit_email_address" readonly="">
                </div>  
            </div>
            <div class="col-md-5">
                <div class="form-group ">
                    <label class="control-label">User Role</label>
                    <select class="form-control" name="role" id="edit_role" required>
                        <option value='' selected=""></option>
                        <option value="1">Administrator</option>
                        <option value="2">Encoder</option>
                        <option value="3">Student</option>
                    </select>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Password</label>
                    <input type="password" class="form-control" name="password" id="password"
                    minlength="6" maxlength="20" >
                </div>  
            </div>
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="control-label">Confirm Password</label>
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password" minlength="6" maxlength="20" >
                </div>  
            </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <center><button type="submit" class="btn btn-success">Confirm</button></center>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>
<script type="text/javascript">
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
set_csrf('<?php echo $this->security->get_csrf_hash()?>');
	function get_users()
    {
       $('#users').DataTable({
      		"ajax": "<?php echo base_url('Users/getRecords')?>",
            "deferRender": true,
            "stateSave": true,
            "order":[],/*
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            },*/
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
    
	$(function($) {
		get_users();
		 var table = $('#users').DataTable();
		table.on('click', '.activate, .deactivate', function(e)
		{
			var id = $(this).data('id');
	        var post_url = '<?php echo base_url()?>Users/update_status';
	        swal({
			  title: 'Are you sure?',
			  text: "The account status will be updated",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirm'
			}).then((result) => {
			  	$.ajax({
		            type : 'POST',
		            url : post_url,
		            data:{id: id},
		            dataType:"json",
		            beforeSend:function(){
		            	loading();
		            },
		            success : function(res){
		            	close_loading();
		                if(res.message=="success")
		                {
		                    $("#users").dataTable().fnDestroy();
		                    get_users();
		                    notify2("Success","User status updated successfuly","success");
		                }
		                else
		                {
		                    notify2("Failed","User status update failed","error");
		                }

		            },
		            error : function() {
		                notify2("Failed","User status update failed","error");
		            }
		        });
			});
	        
		});

        table.on('click', '.modify', function(e)
        {
            e.preventDefault();
            var id = $(this).data('id');
            var post_url = '<?php echo base_url()?>Users/get_user';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:{id: id},
                dataType:"json",
                beforeSend:function(){
                },
                success : function(res){
                    $("#edit_id").val(id);
                    $("#edit_first_name").val(res.user.first_name);
                    $("#edit_last_name").val(res.user.last_name);
                    $("#edit_middle_name").val(res.user.middle_name);
                    $("#edit_suffix").val(res.user.suffix);
                    $("#edit_address").val(res.user.address);
                    $("#edit_contact_number").val(res.user.contact_number);
                    $("#edit_role").val(res.user.role);
                    $("#edit_email_address").val(res.user.email_address);
                },
                error : function() {
                    $('#modal_content').html('<p class="error">Error in submit</p>');
                }
            });
        });

        $("#modify_user").on("submit",function(e)
        {
            e.preventDefault();
            var post_url = '<?php echo base_url()?>Users/update_user';
            $.ajax({
                type : 'POST',
                url : post_url,
                data:$("#modify_user").serialize(),
                dataType:"json",
                beforeSend:function(){
                    loading();
                },
                success : function(res){
                    close_loading();
                    if(res.message=="success")
                    {
                        $("#users").dataTable().fnDestroy();
                        get_users();
                        notify2("Success","User updated successfuly","success");
                        $("#userModal").modal("toggle");
                    }
                    else
                    {
                        notify2("Failed","User update falied","error");
                    }

                },
                error : function() {
                    notify2("Failed","User status update falied","error");
                }
            });
        })
  	});


</script>