<style type="text/css">

</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                    <h4>Add Exam Result</h4>
                    </div>
                    <div class="card-content">
                        <?php echo form_open('Exams/add','method="POST" id="add_exam" autocomplete="off"') ?>
                        <h4 class="card-title">Examinee Information</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">First Name</label>
                                        <input type="text" class="form-control" name="first_name" required>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Middle Name</label>
                                        <input type="text" class="form-control" name="middle_name">
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Suffix</label>
                                        <input type="text" class="form-control" name="suffix">
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name of School</label>
                                        <input type="text" class="form-control" name="school" required>
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Contact Number</label>
                                        <input type="number" class="form-control" name="contact_number" required>
                                    </div>  
                                </div>
                            </div>
                        <h4 class="card-title">Exam Information</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Score/Rating</label>
                                        <input type="number" class="form-control" name="rating" required>
                                    </div>  
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Date of Examination</label>
                                        <input type="date" class="form-control date" name="date" required>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Academic Year From</label>
                                        <input type="number" class="form-control" name="academic_year_from" 
                                        min="2000"  required>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" style="text-align:center;">
                                        <label class="control-label" style="font-size:14px;">TO</label>
                                    </div>  
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Academic Year To</label>
                                        <input type="number" class="form-control" name="academic_year_to"  min="2000" required>
                                    </div>  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-fill btn-success " name="submit">Submit</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#add_exam").on("submit",function(e)
    {
        e.preventDefault();
        var post_url = '<?php echo base_url("Exams/add")?>';
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#add_exam').serialize(),
            dataType:"json",
            beforeSend:function(){
                loading();
            },
            success : function(res){
                close_loading();
                if(res.message=="success")
                    notify2("Success","Exam added successfully","success");
                else
                    notify2("Failed","Exam added failed","error");

                $('#add_exam').each(function() { this.reset() });
            },
            error : function() {
                notify2("Failed","Exam added failed","error");
            }
        });
    });
</script>