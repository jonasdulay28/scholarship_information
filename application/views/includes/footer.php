<footer class="footer" style="position:relative;bottom:0;">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="<?php echo base_url()?>">
                        Home
                    </a>
                </li>
                <?php if(decrypt($this->session->role) == 1){ ?>
                <li>
                    <a href="<?php echo base_url('Users')?>">
                        Users
                    </a>
                </li>
                <?php } ?>
                <li>
                    <a href="<?php echo base_url('Exams')?>">
                        Exams
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('Monitoring')?>">
                        Monitoring 
                    </a>
                </li>
                <?php if(decrypt($this->session->role) == 1){ ?>
                <li>
                    <a href="<?php echo base_url('Curriculum/records')?>">
                        Curriculum 
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('Scholarship/scholarship')?>">
                        Scholarship 
                    </a>
                </li>
                <?php } ?>
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="<?php echo base_url()?>">St. Joseph School</a>, Scholarship Information System
        </p>
    </div>
</footer>