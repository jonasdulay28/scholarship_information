<div class="sidebar" data-color="green" data-image="<?php echo base_url()?>material/assets/img/sidebar-1.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="<?php echo base_url()?>" class="simple-text">
            ST. Joseph School
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li <?php if($segment1=="dashboard" || $segment1==""){echo 'class="active"';}?>>
                <a href="<?php echo base_url('Dashboard');?>">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <?php if(decrypt($this->session->role) == 1){ ?>
            <li <?php if($segment1=="users"){echo 'class="active"';}?>>
                <a data-toggle="collapse" href="#users_menu" <?php if($segment1=="users"){echo 'aria-expanded="true"';}?>>
                    <i class="material-icons">person</i>
                    <p> User Management
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php if($segment1=="users"){echo 'in';}?>" id="users_menu"
                     <?php if($segment1=="users"){echo 'aria-expanded="true"';}?>>
                    <ul class="nav">
                        <li>
                            <a href="<?php echo base_url('Users/add');?>" class="sub-nav">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal"> Add User </span>
                            </a>
                        </li>
                        <li >
                            <a href="<?php echo base_url('Users/records');?>" class="sub-nav">
                                <span class="sidebar-mini"> </span>
                                <span class="sidebar-normal"> View Users </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php } ?>
            <li <?php if($segment1=="exams"){echo 'class="active"';}?>>
                <a data-toggle="collapse" href="#exams_menu" <?php if($segment1=="exams"){echo 'aria-expanded="true"';}?>>
                    <i class="fa fa-list-alt"></i>
                    <p> Examination Result
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php if($segment1=="exams"){echo 'in';}?>" id="exams_menu"
                     <?php if($segment1=="exams"){echo 'aria-expanded="true"';}?>>
                    <ul class="nav">
                        <li>
                            <a href="<?php echo base_url('Exams/add');?>" class="sub-nav">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal"> Add Exam Result </span>
                            </a>
                        </li>
                        <li >
                            <a href="<?php echo base_url('Exams/records');?>" class="sub-nav">
                                <span class="sidebar-mini"> </span>
                                <span class="sidebar-normal"> View Exam Results </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php if(decrypt($this->session->role) == 1){ ?>
            <li <?php if($segment1=="curriculum"){echo 'class="active"';}?>>
                <a data-toggle="collapse" href="#curriculum_menu" <?php if($segment1=="curriculum"){echo 'aria-expanded="true"';}?>>
                    <i class="fa fa-book"></i>
                    <p> Curriculum
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php if($segment1=="curriculum"){echo 'in';}?>" id="curriculum_menu"
                     <?php if($segment1=="curriculum"){echo 'aria-expanded="true"';}?>>
                    <ul class="nav">
                        <li>
                            <a href="<?php echo base_url('Curriculum/add');?>" class="sub-nav">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal"> Add Curriculum </span>
                            </a>
                        </li>
                        <li >
                            <a href="<?php echo base_url('Curriculum/records');?>" class="sub-nav">
                                <span class="sidebar-mini"> </span>
                                <span class="sidebar-normal"> View Curriculums </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php } ?>
            <li <?php if($segment1=="monitoring"){echo 'class="active"';}?>>
                <a href="<?php echo base_url('Monitoring/');?>">
                    <i class="fa fa-desktop"></i>
                    <p>Monitoring</p>
                </a>
            </li>
            <?php if(decrypt($this->session->role) == 1){ ?>
            <li <?php if($segment1=="scholarship"){echo 'class="active"';}?>>
                <a href="<?php echo base_url('Scholarship/scholarship');?>">
                    <i class="fa fa-graduation-cap"></i>
                    <p>Scholarship</p>
                </a>
            </li>

            <li <?php if($segment1=="create_letter"){echo 'class="active"';}?>>
                <a href="<?php echo base_url('Create_Letter/index');?>">
                    <i class="fa fa-envelope"></i>
                    <p>Create Letter</p>
                </a>
            </li>
            
            <li <?php if($segment1=="settings"){echo 'class="active"';}?>>
                <a data-toggle="collapse" href="#settings_menu" <?php if($segment1=="settings"){echo 'aria-expanded="true"';}?>>
                    <i class="material-icons">settings </i>
                    <p> Settings
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php if($segment1=="settings"){echo 'in';}?>" id="settings_menu"
                     <?php if($segment1=="settings"){echo 'aria-expanded="true"';}?>>
                    <ul class="nav">
                        <li>
                            <a href="<?php echo base_url('Settings/audit_trails');?>" class="sub-nav">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal"> Audit Trails </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('Backup/index');?>" class="sub-nav">
                                <span class="sidebar-mini"></span>
                                <span class="sidebar-normal"> Backup Database </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php } ?>
            
        </ul>
    </div>
</div>