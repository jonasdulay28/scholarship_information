<section class="content-header">
  <h1>
    <span class="fa fa-user"></span> User Management
    <small>dashboard</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
    <li class="active"><a href="#"><span class="fa fa-user"></span> User Management</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="btn-inline">  
        <button type="button"  class="btn btn-primary " id="add_user" data-backdrop="static"><span class="fa fa-plus"></span>  Add account</button>
        <div class="btn-group">
            <button type="button" class="btn btn-primary" id="btn_sort_all_users" ><span class="fa fa-sort"></span> All</button>
            <button type="button" class="btn btn-primary" id="btn_sort_active_users" data-column="status" data-value="1"><span class="fa fa-sort"></span> Active</button>
            <button type="button" class="btn btn-primary" id="btn_sort_inactive_users" data-column="status" data-value="0"><span class="fa fa-sort"></span> Inactive</button>
                                
          <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="fa fa-sort"></span>  Role <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
            <?php foreach($user_roles as $key){ ?>
              <li><a href="#" data-value = "<?php echo $key['id']?>" data-column="role" class="btn_sort_role"><?php echo $key['role_name']?></a></li>
            <?php } ?>
            </ul>
          </div>
        </div>
    </div>
  <br>
        <div class="box">
            <div class="box-body">
                <table id="users" class="table table-bordered  table-striped table-responsive  display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Row</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Role</th>
                            <th>Branch</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Row</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Role</th>
                            <th>Branch</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
  <br>
</section><!-- /.content -->

  <!-- Add Account Modal -->
  <div class="modal fade" id="add_user_modal" role="dialog">
    <div class="modal-dialog ">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="fa fa-plus"></span> Add User</h4>
        </div>
        <div class="modal-body">
          <div id="modal_content"></div>
          <?php echo form_open('','id="add_user_form"'); ?>
            <div id="error"></div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <input type="text" name="name" class="form-control " placeholder="Full name" required>
                </div>
            </div>
            <br>
            <input type="email" name="email" class="form-control" placeholder="Email Address" required>
            <br>
            <div class="row">
                <div class="col-sm-6 col-md-6 paddingr2">
                    <input type="text" name="address" class="form-control" placeholder="Address" required>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">+63</span>
                        <input type="number" name="contact_num" class="form-control" placeholder="Contact Number"  required>
                    </div>
                </div>
            </div>
            <br>
            <select name="role" class="form-control" required>
                <option value="">-- Please Select a Role --</option>
              <?php foreach($user_roles as $key){ ?>
              <option value="<?php echo encrypt($key["id"]) ?>"><?php echo $key["role_name"] ?></option>
              <?php } ?>
            </select>
            <br>
            <select name="branch" class="form-control" required>
                <option value="">-- Please Select a Branch --</option>
              <?php foreach($branches as $key){ ?>
              <option value="<?php echo encrypt($key["id"]) ?>"><?php echo $key["branch_name"] ?></option>
              <?php } ?>
            </select>
            <br>
            <div class="modal-footer">
                <center>
                    <div id="feedback"></div>
                    <a href="javascript:void(0);" class="text-muted"><button type="submit" class="btn btn-primary" id="add_user_btn"><span class="fa fa-check"></span> Add account</button></a>
                </center>
            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
      
    </div>
  </div>

<!-- Edit Account Modal -->
<div class="modal fade" id="update_user_modal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="fa fa-pencil"></span> Edit User Information  </h4>
        </div>
        <div class="modal-body">
          <div id="edit_modal_content"></div>
          <?php echo form_open('','id="edit_user_form"'); ?>
            <div id="error"></div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <input type="text" name="name" class="form-control " id="edit_name" placeholder="Full name" >
                </div>
            </div>
            <br>
            <input type="email" name="email" class="form-control"  id="edit_email" placeholder="Email Address" disabled>
            <br>
            <div class="row">
                <div class="col-sm-6 col-md-6 paddingr2">
                    <input type="text" name="address" class="form-control"  id="edit_address" placeholder="Address" required>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon">+63</span>
                        <input type="number" name="contact_num" class="form-control"  id="edit_contact" placeholder="Contact Number"  required>
                    </div>
                </div>
            </div>
            <br>
            <select name="role" class="form-control"   id="edit_role"required>
                <option value="">-- Please Select a Role --</option>
              <?php foreach($user_roles as $key){ ?>
              <option value="<?php echo encrypt($key["id"]) ?>"><?php echo $key["role_name"] ?></option>
              <?php } ?>
            </select>
            <br>
            <select name="branch" class="form-control" id="edit_branch" required>
                <option value="">-- Please Select a Branch --</option>
              <?php foreach($branches as $key){ ?>
              <option value="<?php echo encrypt($key["id"]) ?>"><?php echo $key["branch_name"] ?></option>
              <?php } ?>
            </select>
            <br>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6" id="created"></div>
                <div class="col-xs-6 col-sm-6 col-md-6" id="modified"></div>
            </div>
            <br>
            <input type="hidden" id="edit_oauth_uid" name="oauth_uid" required>
            <div class="modal-footer">
                <center>
                    <div id="edit_feedback"></div>
                    <a href="javascript:void(0);" class="text-muted">
                        <button type="submit" class="btn btn-primary" id="update_user_btn"><span class="fa fa-check"></span> Update account</button>
                    </a>
                </center>
            </div>
            <?php echo form_close()?>
        </div>
      </div>
              
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#add_user").click(function(){
        $("#add_user_modal").modal({
          "backdrop": "static" 
        });

    });
    function get_users()
    {
       $('#users').DataTable( {
            "ajax": "<?php echo base_url()?>Pabile_Users/get_users",
            "deferRender": true,
            "stateSave": true,
            "order":[],
            "columnDefs": [ {
            "targets": 8,
            "orderable": false
            } ]
        } ); 
    }

    get_users();

    $('#add_user_form').submit(function(e){
        e.preventDefault();
        var post_url = '<?php echo base_url()?>Pabile_Users/add_user';
        $.ajaxSetup({data: {token: CFG.token}});
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#add_user_form').serialize(),
            dataType:"json",
            beforeSend:function(){
               $('#feedback').html('<center><img src="<?php echo base_url()?>assets/img/loading_dots.gif" height="50px"></center>');
               $('#add_user_btn').hide();
            },
            success : function(res){
                $('#feedback').html('');
                $('#add_user_btn').show(500);
                if(res.error=="")
                {
                    $('#add_user_form')[0].reset();
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    $('#add_user_modal').modal('toggle');
                    $("#users").dataTable().fnDestroy();
                    notify("User added successfuly","success");
                    get_users();
                }
                else
                {
                    $('#modal_content').html(res.message);
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    notify("Error. Try again.","danger");
                }
                
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });

    $(document).on('click','.update_user_btn',function(e)
    {
        e.preventDefault();
        var oauth_uid = $(this).data('id');
        var post_url = '<?php echo base_url()?>Pabile_Users/get_user';
        $.ajaxSetup({data: {token: CFG.token}});
        $.ajax({
            type : 'POST',
            url : post_url,
            data:{oauth_uid: oauth_uid},
            dataType:"json",
            beforeSend:function(){
            },
            success : function(res){
                console.log(res);   
                var role = ""
                $('input:hidden[name="token"]').val(res.token);
                $.ajaxSetup({data: {token: res.token}});         
                $("#edit_name").val(res.user.name);
                $("#edit_email").val(res.user.email);
                $("#edit_address").val(res.user.address);
                $("#edit_contact").val(res.user.contact_number);
                $("#edit_role").val(res.user.role_id);
                $("#edit_branch").val(res.user.branch_id);
                $("#edit_oauth_uid").val(oauth_uid);
                $("#created").html("<label>Created</label><br>"+res.user.created);
                $("#modified").html("<label>Modified</label><br>"+res.user.modified);
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });

    $('#edit_user_form').submit(function(e){
        e.preventDefault();
        var post_url = '<?php echo base_url()?>Pabile_Users/update_user';
        $.ajaxSetup({data: {token: CFG.token}});
        $.ajax({
            type : 'POST',
            url : post_url,
            data: $('#edit_user_form').serialize(),
            dataType:"json",
            beforeSend:function(){
               $('#edit_feedback').html('<center><img src="<?php echo base_url()?>assets/img/loading_dots.gif" height="50px"></center>');
               $('#update_user_btn').hide();
            },
            success : function(res){
                $('#edit_feedback').html('');
                $('#update_user_btn').show(500);
                if(res.error=="")
                {
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    $('#update_user_modal').modal('toggle');
                    $("#users").dataTable().fnDestroy();
                    get_users();
                    notify("User updated successfuly","success");
                }
                else
                {
                    $('#edit_modal_content').html(res.message);
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    notify("Failed","danger");
                }
                
            },
            error : function() {
                $('#modal_content').html('<p class="error">Error in submit</p>');
            }
        });
    });

    $(document).on('click','.update_status',function(e)
    {
        e.preventDefault();
        var oauth_uid = $(this).data('id');
        var post_url = '<?php echo base_url()?>Pabile_Users/update_status';
        $.ajaxSetup({data: {token: CFG.token}});
        $.ajax({
            type : 'POST',
            url : post_url,
            data:{oauth_uid: oauth_uid},
            dataType:"json",
            beforeSend:function(){
            },
            success : function(res){
                if(res.error=="")
                {
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    $("#users").dataTable().fnDestroy();
                    get_users();
                    notify("User status updated successfuly","success");
                }
                else
                {
                    $('input:hidden[name="token"]').val(res.token);
                    $.ajaxSetup({data: {token: res.token}});
                    notify("Failed","danger");
                }

            },
            error : function() {
                notify("Failed","danger");
            }
        });
    });
});
</script>