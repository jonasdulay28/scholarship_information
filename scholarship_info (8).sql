-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2018 at 09:33 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scholarship_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_trails`
--

CREATE TABLE `audit_trails` (
  `id` int(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_trails`
--

INSERT INTO `audit_trails` (`id`, `action`, `description`, `user_id`, `ip_address`, `created`) VALUES
(1, 'sadas', 'dsadasdas', 1, 'asdasd', '2018-03-19 01:21:26'),
(2, 'Logged in', 'Logged in', 1, '127.0.0.1', '2018-03-19 07:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `curriculum`
--

CREATE TABLE `curriculum` (
  `id` int(255) NOT NULL,
  `curriculum` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `subjects` varchar(2000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `curriculum`
--

INSERT INTO `curriculum` (`id`, `curriculum`, `description`, `subjects`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Grade 7 (2018)', 'test', 'English;Filipino;Science &amp Technology;Science and Technology;MAPEH', 1, '2018-03-16 02:25:03', '2018-03-19 00:00:20');

-- --------------------------------------------------------

--
-- Table structure for table `examinations`
--

CREATE TABLE `examinations` (
  `id` int(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `suffix` varchar(100) DEFAULT NULL,
  `school` varchar(255) NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `rating` int(10) NOT NULL,
  `exam_date` varchar(50) NOT NULL,
  `academic_year_from` varchar(20) NOT NULL,
  `academic_year_to` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examinations`
--

INSERT INTO `examinations` (`id`, `first_name`, `last_name`, `middle_name`, `suffix`, `school`, `contact_number`, `rating`, `exam_date`, `academic_year_from`, `academic_year_to`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Jonas', 'Dulay', 'Reolada', '', 'FEU Institute Of Technology', '09206008945', 89, '03/16/2018', '2017', '2018', '2018-03-16 02:26:29', '2018-03-16 02:26:36', 1),
(2, 'Jonas2', 'Dulay2', 'Reolada2', '', 'FEU Institute Of Technologysd', '09206008945', 89, '03/16/2018', '2017', '2018', '2018-03-16 02:26:29', '2018-03-16 02:26:36', 1),
(3, 'Jonas2', 'Dula3', 'Reolada3', '', 'FEU Institute Of Technologysd', '09206008945', 89, '03/16/2018', '2017', '2018', '2018-03-16 02:26:29', '2018-03-16 02:26:36', 1),
(4, 'Jonas2', 'Dula3', 'Reolada3', '', 'FEU Institute Of Technologysdqweqwe', '09206008945', 89, '03/16/2018', '2017', '2018', '2018-03-16 02:26:29', '2018-03-16 02:26:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholars`
--

CREATE TABLE `scholars` (
  `id` int(255) NOT NULL,
  `examinations_id` int(255) NOT NULL,
  `scholarship_type` int(255) NOT NULL,
  `student_number` varchar(20) DEFAULT NULL,
  `curriculum_id` int(255) DEFAULT NULL,
  `section` varchar(500) DEFAULT NULL,
  `academic_year_from` varchar(100) NOT NULL,
  `academic_year_to` varchar(100) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholars`
--

INSERT INTO `scholars` (`id`, `examinations_id`, `scholarship_type`, `student_number`, `curriculum_id`, `section`, `academic_year_from`, `academic_year_to`, `contact_number`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 2, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-16 02:26:58', 1),
(3, 2, 1, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-16 02:26:58', 1),
(4, 3, 1, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-16 02:26:58', 1),
(5, 4, 3, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-16 02:26:58', 1),
(6, 5, 3, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-18 23:53:27', 1),
(7, 6, 3, '201311357', 1, 'Tste', '2017', '2018', '09206008945', '2018-03-15 16:00:00', '2018-03-16 02:26:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholarship`
--

CREATE TABLE `scholarship` (
  `id` int(255) NOT NULL,
  `scholar_type` varchar(100) NOT NULL,
  `min_rating` int(20) NOT NULL,
  `max_rating` int(20) NOT NULL,
  `color` varchar(50) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship`
--

INSERT INTO `scholarship` (`id`, `scholar_type`, `min_rating`, `max_rating`, `color`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Full Scholarship', 91, 100, 'green', 1, '2018-03-10 16:44:24', '2018-03-18 23:55:57'),
(2, '50% Scholarship', 88, 90, 'orange', 1, '2018-03-10 16:44:24', '2018-03-17 23:54:33'),
(3, 'Not Qualified', 75, 87, 'red', 1, '2018-03-10 16:49:53', '2018-03-17 23:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_grades`
--

CREATE TABLE `scholar_grades` (
  `id` int(255) NOT NULL,
  `subjects` text NOT NULL,
  `grades` text NOT NULL,
  `latest_avg` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `scholar_id` int(255) NOT NULL,
  `curriculum_id` int(255) NOT NULL,
  `section` varchar(250) DEFAULT NULL,
  `academic_year` varchar(100) NOT NULL,
  `scholar_type` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_grades`
--

INSERT INTO `scholar_grades` (`id`, `subjects`, `grades`, `latest_avg`, `status`, `scholar_id`, `curriculum_id`, `section`, `academic_year`, `scholar_type`, `created_at`, `updated_at`) VALUES
(1, 'English;Filipino;Science &amp; Technology;Science and Technology;MAPEH;Final Avg.', '85;100;95;85;91.25;100;0;0;0;25;90;0;0;0;22.5;100;0;0;0;25;95;0;0;0;23.75;100;0;0;0;25;95.00;16.67;15.83;14.17;35.42', '35.42', 0, 1, 1, 'Tste', '2017-2018', '50% Scholarship', '2018-03-18 08:03:39', '2018-03-19 00:41:34'),
(6, 'English;Filipino;Science &amp; Technology;Science and Technology;MAPEH;Final Avg.', '65;0;0;0;16.25;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;10.83;0.00;0.00;0.00;2.71', '2.71', 0, 3, 1, 'Tste', '2017-2018', 'Full Scholarship', '2018-03-18 23:03:42', '2018-03-18 23:21:33');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(255) NOT NULL,
  `section_name` varchar(200) NOT NULL,
  `grade_level` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `suffix` varchar(10) DEFAULT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `suffix`, `address`, `contact_number`, `email_address`, `password`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Diana Jean', 'Malle', 'Mico', '', 'San Pablo', '9206008947', 'admin@gmail.com', '$2y$11$ld19y/Rtn1uRBayjpW8i8O/KbRG05PJaCXZeOqm2xO7UicYB2UVDm', 1, 1, '2018-03-10 08:41:31', '2018-03-12 06:26:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_trails`
--
ALTER TABLE `audit_trails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculum`
--
ALTER TABLE `curriculum`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curriculum` (`curriculum`);

--
-- Indexes for table `examinations`
--
ALTER TABLE `examinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholars`
--
ALTER TABLE `scholars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `examinations_id` (`examinations_id`);

--
-- Indexes for table `scholarship`
--
ALTER TABLE `scholarship`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `scholar_type` (`scholar_type`);

--
-- Indexes for table `scholar_grades`
--
ALTER TABLE `scholar_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `scholar_type` (`section_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_trails`
--
ALTER TABLE `audit_trails`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `curriculum`
--
ALTER TABLE `curriculum`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `examinations`
--
ALTER TABLE `examinations`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholars`
--
ALTER TABLE `scholars`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `scholarship`
--
ALTER TABLE `scholarship`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholar_grades`
--
ALTER TABLE `scholar_grades`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
